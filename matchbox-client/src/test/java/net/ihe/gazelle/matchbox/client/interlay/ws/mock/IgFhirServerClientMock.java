package net.ihe.gazelle.matchbox.client.interlay.ws.mock;

import net.ihe.gazelle.matchbox.client.interlay.ws.IGFhirServerClientImpl;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

public class IgFhirServerClientMock extends IGFhirServerClientImpl {

    String passedResponsePath = "src/test/resources/post_response.json";
    String failedResponsePath = "src/test/resources/post_response_failed.json";


    /**
     * Default constructor for the class.
     *
     * @param serverUrl URL of the server.
     */
    public IgFhirServerClientMock(String serverUrl) {
        super(serverUrl);
    }

    public String sendMessageToValidation(String messageToValidate, String profile) {
        String response = "";
        File responseFile = new File(getResponse(profile));
        try {
            response = Files.readString(Path.of(responseFile.getAbsolutePath()));

        } catch (Exception e) {
            System.out.println("Problem during readString: " + e);
        }
        return response;
    }


    /**
     * Return the literal value of the message to pur in the report
     *
     * @param e       exception to report
     * @param message message to display with the error
     * @return literal value of the message.
     */
    private String getExceptionReportMessage(String message, Exception e) {
        StringBuilder finalMessage = new StringBuilder(message);
        if (e.getMessage() != null && !e.getMessage().isEmpty()) {
            finalMessage.append(String.format("with message : [ %s ]", e.getMessage()));
        }
        return finalMessage.toString();
    }

    private String getResponse(String profile) {
        return "passed".equals(profile) ? passedResponsePath : failedResponsePath;
    }
}
