package net.ihe.gazelle.matchbox.client.interlay.validation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class CustomPatientServiceFactoryTest {

    CustomPatientServiceFactory factory = new CustomPatientServiceFactory();

    @Test
    void testGetValidationService() {
        Assertions.assertInstanceOf(CustomPatientValidationService.class, factory.getValidationService());
    }


}