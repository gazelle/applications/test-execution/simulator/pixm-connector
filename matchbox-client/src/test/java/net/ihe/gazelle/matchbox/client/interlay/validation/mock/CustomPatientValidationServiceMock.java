package net.ihe.gazelle.matchbox.client.interlay.validation.mock;

import net.ihe.gazelle.matchbox.client.interlay.validation.CustomPatientValidationService;
import net.ihe.gazelle.matchbox.client.interlay.ws.mock.IgFhirServerClientMock;

public class CustomPatientValidationServiceMock extends CustomPatientValidationService {

    protected String getResponseFromFhirValidation(String iti104Patient, String fhirProfile) {
        IgFhirServerClientMock igFhirServerClient = new IgFhirServerClientMock("http://localhost");
        return igFhirServerClient.sendMessageToValidation(iti104Patient, fhirProfile);

    }


}
