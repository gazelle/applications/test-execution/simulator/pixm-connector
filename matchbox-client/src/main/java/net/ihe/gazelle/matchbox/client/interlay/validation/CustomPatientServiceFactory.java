package net.ihe.gazelle.matchbox.client.interlay.validation;

import jakarta.enterprise.inject.Default;
import jakarta.ws.rs.Produces;
import net.ihe.gazelle.validation.api.application.ValidationService;

public class CustomPatientServiceFactory {

    @Produces
    @Default
    public ValidationService getValidationService() {
        return new CustomPatientValidationService();
    }
}
