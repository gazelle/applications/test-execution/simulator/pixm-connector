package net.ihe.gazelle.matchbox.client.interlay.ws;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;


public class IGFhirServerClientImpl {

    private static final Logger LOGGER = LoggerFactory.getLogger(IGFhirServerClientImpl.class);
    private String serverUrl;

    /**
     * Default constructor for the class.
     *
     * @param serverUrl URL of the server.
     */
    public IGFhirServerClientImpl(String serverUrl) {
        this.serverUrl = serverUrl;
    }


    /**
     * Send the message to the server.
     *
     * @param messageToValidate literal content of the message to validate.
     * @param profile           literal value of the url of the profile to validate against.
     * @return the literal value of the response entity.
     */
    public String sendMessageToValidation(String messageToValidate, String profile) {
        String result = null;
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpPost httppost = new HttpPost(serverUrl + "/$validate?profile=" +
                    URLEncoder.encode(profile, StandardCharsets.UTF_8));

            StringEntity stringEntity = new StringEntity(messageToValidate, StandardCharsets.UTF_8);
            httppost.setEntity(stringEntity);
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                try (InputStream inputStream = entity.getContent()) {
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                    result = reader.lines()
                            .collect(Collectors.joining("\n"));
                    reader.close();
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error contacting remote FHIR Server for validation.", e);
            result = getExceptionReportMessage(messageToValidate, e);
        }
        return result;
    }


    /**
     * Return the literal value of the message to pur in the report
     *
     * @param e       exception to report
     * @param message message to display with the error
     * @return literal value of the message.
     */
    private String getExceptionReportMessage(String message, Exception e) {
        StringBuilder finalMessage = new StringBuilder(message);
        if (e.getMessage() != null && !e.getMessage().isEmpty()) {
            finalMessage.append(String.format("with message : [ %s ]", e.getMessage()));
        }
        return finalMessage.toString();
    }
}
