package net.ihe.gazelle.matchbox.client.interlay.validation;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import net.ihe.gazelle.matchbox.client.interlay.ws.IGFhirServerClientImpl;
import net.ihe.gazelle.validation.api.application.ValidationService;
import net.ihe.gazelle.validation.api.domain.metadata.structure.ValidationProfile;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationReport;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationTestResult;
import net.ihe.gazelle.validation.api.domain.request.structure.ValidationItem;
import net.ihe.gazelle.validation.api.domain.request.structure.ValidationRequest;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.OperationOutcome;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class CustomPatientValidationService implements ValidationService {
    public static final String MATCHBOX_VALIDATION = "matchbox-validation";
    public static final String APP_IG_FHIR_SERVER = System.getenv("APP_IG_FHIR_SERVER");
    private final ValidationReport validationReport = new ValidationReport();
    List<ValidationItem> validationItemList = new ArrayList<>();

    @Override
    public ValidationReport validate(ValidationRequest validationRequest) {
        ByteArrayInputStream bais = new ByteArrayInputStream(validationRequest.getValidationItems().get(0).getContent());
        String iti104Patient = new String(bais.readAllBytes(), StandardCharsets.UTF_8);
        String response = getResponseFromFhirValidation(iti104Patient, validationRequest.getValidationProfileId());
        addMessageToValidationItem(response);
        hasAnyErrorsInIssues(response);
        validationReport.setValidationItems(validationItemList);
        return validationReport;
    }

    private void addMessageToValidationItem(String message) {
        ValidationItem vi = new ValidationItem();
        vi.setItemId(MATCHBOX_VALIDATION);
        vi.setContent(message.getBytes(StandardCharsets.UTF_8));
        validationItemList.add(vi);
    }

    protected String getResponseFromFhirValidation(String iti104Patient, String fhirProfile) {
        IGFhirServerClientImpl igFhirServerClient = new IGFhirServerClientImpl(APP_IG_FHIR_SERVER);
        return igFhirServerClient.sendMessageToValidation(iti104Patient, fhirProfile);

    }

    private <T extends IBaseResource> T convertStringIntoFhirResource(Class<T> tClass, String input) {
        FhirContext ctx = FhirContext.forR4();
        IParser parser = ctx.newJsonParser();
        return parser.parseResource(tClass, input);
    }

    private void hasAnyErrorsInIssues(String response) {
        OperationOutcome operationOutcome = convertStringIntoFhirResource(OperationOutcome.class, response);
        validationReport.setOverallResult(ValidationTestResult.PASSED);
        for (OperationOutcome.OperationOutcomeIssueComponent issue : operationOutcome.getIssue()) {
            if (isErrorOrFatal(issue.getSeverity())) {
                validationReport.setOverallResult(ValidationTestResult.FAILED);
                return;
            }
        }
    }

    private boolean isErrorOrFatal(OperationOutcome.IssueSeverity severity) {
        return switch (severity) {
            case FATAL, ERROR, NULL -> true;
            default -> false;
        };
    }

    public List<ValidationProfile> getValidationProfiles() {
        return new ArrayList<>();
    }

}
