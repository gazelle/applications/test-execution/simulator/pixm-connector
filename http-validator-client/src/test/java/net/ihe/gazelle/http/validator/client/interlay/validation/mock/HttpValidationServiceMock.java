package net.ihe.gazelle.http.validator.client.interlay.validation.mock;

import net.ihe.gazelle.http.validator.client.interlay.validation.HttpValidationService;
import net.ihe.gazelle.http.validator.client.interlay.validation.ws.mock.HttpValidatorServerClientImplMock;

public class HttpValidationServiceMock extends HttpValidationService {

    protected String getResponseFromHttpValidation(String content) {
        HttpValidatorServerClientImplMock httpValidatorServerClient = new HttpValidatorServerClientImplMock(System.getenv("http://localhost"));
        return httpValidatorServerClient.sendMessageToValidation(content);
    }


}
