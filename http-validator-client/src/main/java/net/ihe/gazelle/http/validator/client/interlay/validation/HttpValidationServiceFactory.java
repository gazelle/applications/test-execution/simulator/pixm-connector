package net.ihe.gazelle.http.validator.client.interlay.validation;

import jakarta.enterprise.inject.Default;
import jakarta.ws.rs.Produces;
import net.ihe.gazelle.validation.api.application.ValidationService;

public class HttpValidationServiceFactory {

    @Produces
    @Default
    public ValidationService getValidationService() {
        return new HttpValidationService();
    }
}
