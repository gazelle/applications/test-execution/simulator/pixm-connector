package net.ihe.gazelle.http.validator.client.interlay.ws;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.stream.Collectors;


public class HttpValidatorServerClientImpl {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpValidatorServerClientImpl.class);
    private String serverUrl;

    /**
     * Default constructor for the class.
     *
     * @param serverUrl URL of the server.
     */
    public HttpValidatorServerClientImpl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    /**
     * Send the message to the server.
     *
     * @param messageToValidate literal content of the message to validate.
     * @return the literal value of the response entity.
     */
    public String sendMessageToValidation(String messageToValidate) {
        String result = null;
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpPost httppost = new HttpPost(serverUrl + "/rest/validation/validate");
            StringEntity stringEntity = new StringEntity(messageToValidate,  StandardCharsets.UTF_8);
            stringEntity.setContentType(ContentType.APPLICATION_JSON.getMimeType());
            httppost.setEntity(stringEntity);
            HttpResponse response = httpclient.execute(httppost);
            // reponse
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                try (InputStream inputStream = entity.getContent()) {
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(inputStream, StandardCharsets.UTF_8));
                    result = reader.lines()
                            .collect(Collectors.joining("\n"));
                    reader.close();
                }
            }
        } catch (IOException e) {
            LOGGER.error("Error contacting remote HTTP Validator Server for validation.", e);
        }
        return result;
    }
}
