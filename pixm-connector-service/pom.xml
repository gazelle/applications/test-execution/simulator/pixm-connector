<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>net.ihe.gazelle</groupId>
        <artifactId>pixm-connector</artifactId>
        <version>3.0.2-SNAPSHOT</version>
    </parent>

    <artifactId>pixm-connector-service</artifactId>
    <name>PIXm Connector Service</name>
    <version>3.0.2-SNAPSHOT</version>
    <packaging>war</packaging>

    <properties>
        <maven.compiler.source>17</maven.compiler.source>
        <maven.compiler.target>17</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <app.patient.registry.version>2.2.0</app.patient.registry.version>
    </properties>

    <build>
        <finalName>pixm-connector</finalName>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-release-plugin</artifactId>
                <version>${maven.release.plugin.version}</version>
                <configuration>
                    <tagNameFormat>@{project.version}</tagNameFormat>
                    <autoVersionSubmodules>true</autoVersionSubmodules>
                    <releaseProfiles>release</releaseProfiles>
                </configuration>
            </plugin>
            <!-- Tell Maven which Java source version you want to use -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven.compiler.plugin.version}</version>
                <configuration>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>${maven.surefire.plugin.version}</version>
                <configuration>
                    <properties>
                        <property>
                            <name>listener</name>
                            <value>io.qameta.allure.junit5.AllureJunit5</value>
                        </property>
                    </properties>
                    <dependenciesToScan>
                        <dependency>net.ihe.gazelle:lib.unit-test</dependency>
                    </dependenciesToScan>
                    <testFailureIgnore>false</testFailureIgnore>
                    <argLine>
                        -javaagent:"${settings.localRepository}/org/aspectj/aspectjweaver/${aspectj.version}/aspectjweaver-${aspectj.version}.jar"
                        --add-opens=java.base/java.lang=ALL-UNNAMED
                        --add-opens=java.base/java.util=ALL-UNNAMED
                    </argLine>
                    <systemProperties>
                        <property>
                            <name>junit.jupiter.extensions.autodetection.enabled</name>
                            <value>true</value>
                        </property>
                        <property>
                            <name>allure.results.directory</name>
                            <value>${project.basedir}/target/allure-results</value>
                        </property>
                    </systemProperties>
                    <argLine>${argLine}</argLine>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>org.junit.jupiter</groupId>
                        <artifactId>junit-jupiter</artifactId>
                        <version>${junit.jupiter.version}</version>
                    </dependency>
                    <dependency>
                        <groupId>org.junit.jupiter</groupId>
                        <artifactId>junit-jupiter-api</artifactId>
                        <version>${junit.jupiter.version}</version>
                    </dependency>
                    <dependency>
                        <groupId>org.junit.jupiter</groupId>
                        <artifactId>junit-jupiter-engine</artifactId>
                        <version>${junit.jupiter.version}</version>
                    </dependency>
                    <dependency>
                        <groupId>uk.org.webcompere</groupId>
                        <artifactId>system-stubs-jupiter</artifactId>
                        <version>${system.stubs.jupiter.version}</version>
                    </dependency>
                    <dependency>
                        <groupId>org.aspectj</groupId>
                        <artifactId>aspectjweaver</artifactId>
                        <version>${aspectj.version}</version>
                    </dependency>
                </dependencies>
            </plugin>
            <plugin>
                <groupId>io.qameta.allure</groupId>
                <artifactId>allure-maven</artifactId>
                <version>${allure.maven.version}</version>
                <configuration>
                    <allureDownloadUrl>
                        https://github.com/allure-framework/allure-maven/archive/refs/tags/${allure.maven.version}.zip
                    </allureDownloadUrl>
                </configuration>
            </plugin>

            <!-- The configuration here tells the WAR plugin to include the FHIR Tester
                overlay. You can omit it if you are not using that feature. -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-war-plugin</artifactId>
                <version>3.3.1</version>
                <configuration>
                    <overlays>
                        <overlay>
                            <groupId>ca.uhn.hapi.fhir</groupId>
                            <artifactId>hapi-fhir-testpage-overlay</artifactId>
                        </overlay>
                    </overlays>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco.maven.plugin.version}</version>
                <executions>
                    <execution>
                        <id>pre-unit-test</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>default-report</id>
                        <phase>package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                        <configuration>
                            <dataFile>${project.build.directory}/target/jacoco.exec</dataFile>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <configuration>
                    <doclint>none</doclint>
                </configuration>
            </plugin>
        </plugins>
    </build>

    <dependencies>
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>validation-api</artifactId>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>framework.preferences-model-api</artifactId>
            <version>1.0.1</version>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>framework.operational-preferences-service</artifactId>
            <version>1.0.1</version>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter</artifactId>
            <version>${junit.jupiter.version}</version>
            <scope>test</scope>
        </dependency>

        <!-- This dependency includes the core HAPI-FHIR classes -->
        <dependency>
            <groupId>ca.uhn.hapi.fhir</groupId>
            <artifactId>hapi-fhir-base</artifactId>
        </dependency>

        <dependency>
            <groupId>ca.uhn.hapi.fhir</groupId>
            <artifactId>hapi-fhir-server</artifactId>
        </dependency>

        <!-- At least one "structures" JAR must also be included -->
        <dependency>
            <groupId>ca.uhn.hapi.fhir</groupId>
            <artifactId>hapi-fhir-structures-r4</artifactId>
        </dependency>

        <!-- Used for validation -->
        <dependency>
            <groupId>ca.uhn.hapi.fhir</groupId>
            <artifactId>hapi-fhir-validation-resources-r4</artifactId>
        </dependency>
        <dependency>
            <groupId>com.phloc</groupId>
            <artifactId>phloc-schematron</artifactId>
            <version>${phloc.schematron.version}</version>
        </dependency>

        <!-- This dependency is used for the "FHIR Tester" web app overlay -->
        <dependency>
            <groupId>ca.uhn.hapi.fhir</groupId>
            <artifactId>hapi-fhir-testpage-overlay</artifactId>
            <version>${hapi.fhir.version}</version>
            <type>war</type>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>ca.uhn.hapi.fhir</groupId>
            <artifactId>hapi-fhir-testpage-overlay</artifactId>
            <version>${hapi.fhir.version}</version>
            <classifier>classes</classifier>
            <scope>provided</scope>
        </dependency>

        <!-- HAPI-FHIR uses Logback for logging support. The logback library is
            included automatically by Maven as a part of the hapi-fhir-base dependency,
            but you also need to include a logging library. Logback is used here, but
            log4j would also be fine. -->
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${logback.classic.version}</version>
        </dependency>


        <!-- Needed for JEE/Servlet support -->
        <dependency>
            <groupId>jakarta.platform</groupId>
            <artifactId>jakarta.jakartaee-api</artifactId>
        </dependency>
        <dependency>
            <groupId>jakarta.servlet</groupId>
            <artifactId>jakarta.servlet-api</artifactId>
        </dependency>

        <!-- If you are using HAPI narrative generation, you will need to include
            Thymeleaf as well. Otherwise the following can be omitted. -->
        <dependency>
            <groupId>org.thymeleaf</groupId>
            <artifactId>thymeleaf</artifactId>
            <version>${thymeleaf.version}</version>
        </dependency>
        <dependency>
            <groupId>org.fhir</groupId>
            <artifactId>ucum</artifactId>
        </dependency>
        <dependency>
            <groupId>com.github.ben-manes.caffeine</groupId>
            <artifactId>caffeine</artifactId>
            <version>${caffeine.version}</version>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>app.patient-registry-xref-search-client</artifactId>
            <version>${app.patient.registry.version}</version>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>app.patient-registry-search-client</artifactId>
            <version>${app.patient.registry.version}</version>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>app.patient-registry-feed-client</artifactId>
            <version>${app.patient.registry.version}</version>
        </dependency>
        <!-- Used for CORS support -->
        <dependency>
            <groupId>org.ebaysf.web</groupId>
            <artifactId>cors-filter</artifactId>
            <version>${cors.filter.version}</version>
            <exclusions>
                <exclusion>
                    <artifactId>servlet-api</artifactId>
                    <groupId>javax.servlet</groupId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-core</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-junit-jupiter</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>io.qameta.allure</groupId>
            <artifactId>allure-junit5</artifactId>
            <version>${allure.junit5.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>matchbox-client</artifactId>
            <version>3.0.2-SNAPSHOT</version>
        </dependency>
        <dependency>
            <groupId>net.ihe.gazelle</groupId>
            <artifactId>http-validator-client</artifactId>
            <version>3.0.2-SNAPSHOT</version>
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>uk.org.webcompere</groupId>
            <artifactId>system-stubs-jupiter</artifactId>
            <version>${system.stubs.jupiter.version}</version>
        </dependency>
    </dependencies>

</project>