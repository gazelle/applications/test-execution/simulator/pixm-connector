package net.ihe.gazelle.adapter.connector;

import net.ihe.gazelle.app.patientregistryapi.business.*;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.r4.model.Enumerations;
import org.hl7.fhir.r4.model.HumanName;

import java.util.List;

/**
 * Converter to transform Patient Registry's {@link Patient} to {@link org.hl7.fhir.r4.model.Patient}.
 *
 * @author pvm
 */
public class GazelleRegistryToFhirConverter {

    private static final String URN_PREFIX = "urn:oid:";

    /**
     * private constructor to override the implicit one.
     */
    private GazelleRegistryToFhirConverter() {
    }

    public static org.hl7.fhir.r4.model.Patient convertPatient(Patient registryPatient) throws ConversionException {
        org.hl7.fhir.r4.model.Patient fhirPatient = new org.hl7.fhir.r4.model.Patient();
        fhirPatient.setId(registryPatient.getUuid());
        fhirPatient.setGender(getGenderCode(registryPatient));
        fhirPatient.setBirthDate(registryPatient.getDateOfBirth());
        fhirPatient.setActive(registryPatient.isActive());
        setNames(fhirPatient, registryPatient);
        setCrossIdentifier(fhirPatient, registryPatient);
        setAddresses(fhirPatient, registryPatient);
        setTelecom(fhirPatient, registryPatient);
        return fhirPatient;

    }

    private static void setNames(org.hl7.fhir.r4.model.Patient fhirPatient, Patient registryPatient) throws ConversionException {
        List<PersonName> patientNames = registryPatient.getNames();
        if (patientNames != null) {
            for (PersonName personName : patientNames) {
                fhirPatient.addName(convertRegistryNameToFhirName(personName));
            }
        }
    }

    private static HumanName convertRegistryNameToFhirName(PersonName registryName) throws ConversionException {
        HumanName fhirName = new HumanName();
        if (registryName != null) {
            fhirName.setFamily(registryName.getFamily());
            for (String given : registryName.getGivens()) {
                fhirName.addGiven(given);
            }
            try {
                fhirName.setUse(HumanName.NameUse.fromCode(registryName.getUse()));
            } catch (FHIRException e) {
                throw new ConversionException(String.format("Cannot convert PersonName use : %s", registryName.getUse()), e);
            }
            fhirName.addPrefix(registryName.getPrefix());
            fhirName.addSuffix(registryName.getSuffix());
        }
        return fhirName;
    }

    private static Enumerations.AdministrativeGender getGenderCode(Patient registryPatient) {
        if (registryPatient.getGender() == null) {
            return null;
        } else {
            return switch (registryPatient.getGender()) {
                case MALE -> Enumerations.AdministrativeGender.MALE;
                case FEMALE -> Enumerations.AdministrativeGender.FEMALE;
                case UNDEFINED -> Enumerations.AdministrativeGender.UNKNOWN;
                case OTHER -> Enumerations.AdministrativeGender.OTHER;
            };
        }
    }

    private static void setCrossIdentifier(org.hl7.fhir.r4.model.Patient fhirPatient, Patient registryPatient) {
        if (registryPatient.getIdentifiers() != null) {
            for (EntityIdentifier currentPatientIdentifier : registryPatient.getIdentifiers()) {
                if (currentPatientIdentifier.getSystemIdentifier() != null) {
                    String fhirSystem = getUniversalIDAsUrn(currentPatientIdentifier.getSystemIdentifier());
                    fhirPatient.addIdentifier().setSystem(fhirSystem).setValue(currentPatientIdentifier.getValue());
                }
            }
        }
    }

    private static void setAddresses(org.hl7.fhir.r4.model.Patient fhirPatient, Patient patient) throws ConversionException {
        List<Address> addressList = patient.getAddresses();
        if (addressList != null) {
            for (Address address : addressList) {
                org.hl7.fhir.r4.model.Address fhirAddress = new org.hl7.fhir.r4.model.Address();
                for (String line : address.getLines()) {
                    fhirAddress.addLine(line);
                }
                fhirAddress.setCity(address.getCity());
                fhirAddress.setCountry(address.getCountryIso3());
                fhirAddress.setPostalCode(address.getPostalCode());
                fhirAddress.setState(address.getState());
                fhirAddress.setUse(getAddressUse(address.getUse()));
                fhirPatient.addAddress(fhirAddress);
            }
        }
    }

    private static void setTelecom(org.hl7.fhir.r4.model.Patient fhirPatient, Patient patient) throws ConversionException {
        List<ContactPoint> contactPoints = patient.getContactPoints();
        if (contactPoints != null) {
            for (ContactPoint contactPoint : contactPoints) {
                if (contactPoint != null) {
                    org.hl7.fhir.r4.model.ContactPoint fhirContactPoint = new org.hl7.fhir.r4.model.ContactPoint();
                    fhirContactPoint.setSystem(getContactPointSystem(contactPoint.getType()));
                    fhirContactPoint.setValue(contactPoint.getValue());
                    fhirContactPoint.setUse(getContactPointUse(contactPoint.getUse()));
                    fhirPatient.addTelecom(fhirContactPoint);
                }
            }
        }
    }

    private static String getUniversalIDAsUrn(String universalID) {
        String urnToReturn = null;
        if (universalID != null) {
            urnToReturn = (universalID.startsWith(URN_PREFIX) ? universalID : URN_PREFIX + universalID);
        }
        return urnToReturn;
    }

    private static org.hl7.fhir.r4.model.Address.AddressUse getAddressUse(AddressUse addressUse) throws ConversionException {
        if (addressUse == null) {
            return null;
        }
        return switch (addressUse) {
            case HOME, PRIMARY_HOME -> org.hl7.fhir.r4.model.Address.AddressUse.HOME;
            case WORK -> org.hl7.fhir.r4.model.Address.AddressUse.WORK;
            case VACATION_HOME, TEMPORARY -> org.hl7.fhir.r4.model.Address.AddressUse.TEMP;
            case BAD -> org.hl7.fhir.r4.model.Address.AddressUse.OLD;
            case BILLING -> org.hl7.fhir.r4.model.Address.AddressUse.BILLING;
            default -> throw new ConversionException(String.format("Cannot convert AddressUse : %s", addressUse));
        };

    }

    private static org.hl7.fhir.r4.model.ContactPoint.ContactPointUse getContactPointUse(ContactPointUse contactPointUse)
            throws ConversionException {
        if (contactPointUse == null) {
            return null;
        }
        return switch (contactPointUse) {
            case HOME, PRIMARY_HOME -> org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.HOME;
            case MOBILE -> org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.MOBILE;
            case WORK -> org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.WORK;
            case TEMPORARY -> org.hl7.fhir.r4.model.ContactPoint.ContactPointUse.TEMP;
            default ->
                    throw new ConversionException(String.format("Cannot convert ContactPointUse : %s", contactPointUse.value()));
        };
    }

    private static org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem getContactPointSystem(ContactPointType contactPointType)
            throws ConversionException {
        if (contactPointType == null) {
            return null;
        }
        return switch (contactPointType) {
            case BEEPER -> org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.PAGER;
            case PHONE -> org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.PHONE;
            case FAX -> org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.FAX;
            case URL -> org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.URL;
            case EMAIL -> org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.EMAIL;
            case SMS -> org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.SMS;
            case OTHER -> org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem.OTHER;
            default ->
                    throw new ConversionException(String.format("Cannot convert ContactPointType : %s", contactPointType));
        };

    }

}
