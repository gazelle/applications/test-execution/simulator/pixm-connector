package net.ihe.gazelle.adapter.connector;

import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.GenderCode;
import net.ihe.gazelle.app.patientregistryapi.business.Person;
import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.Address.AddressUse;
import org.hl7.fhir.r4.model.Patient.ContactComponent;
import org.jetbrains.annotations.NotNull;

/**
 * Class containing static methods to convert
 *
 * @author pvm
 */
public class FhirToGazelleRegistryConverter {

    public static final String SOURCE_IDENTIFIER_PATIENT_IDENTIFIER_NOT_FOUND = "sourceIdentifier Patient Identifier not found";
    public static final String SOURCE_IDENTIFIER_PATIENT_IDENTIFIER_NOT_HTTP_NOR_URN_OID = "sourceIdentifier Patient Identifier does not contain http nor urn:oid:";
    public static final String SOURCE_IDENTIFIER_ASSIGNING_AUTHORITY_NOT_FOUND = "sourceIdentifier Assigning Authority not found";
    public static final String ISO = "ISO";
    private static final String URN_OID = "urn:oid:";
    private static final String HTTP_PROTOCOL = "http";

    /**
     * private constructor to override the public implicit one.
     */
    private FhirToGazelleRegistryConverter() {
    }

    public static net.ihe.gazelle.app.patientregistryapi.business.Patient convertPatient(Patient fhirPatient) throws ConversionException {
        net.ihe.gazelle.app.patientregistryapi.business.Patient registryPatient = new net.ihe.gazelle.app.patientregistryapi.business.Patient();
        if (fhirPatient.hasActive()) {
            registryPatient.setActive(fhirPatient.getActive());
        }
        else{
            registryPatient.setActive(true);
        }

        if (fhirPatient.hasBirthDate()) {
            registryPatient.setDateOfBirth(fhirPatient.getBirthDate());
        }

        if (fhirPatient.hasDeceased()) {
            registryPatient.setDateOfDeath(fhirPatient.getDeceasedDateTimeType().getValue());
        }

        if (fhirPatient.hasGender()) {
            registryPatient.setGender(convertGender(fhirPatient.getGender()));
        }

        if (fhirPatient.hasMultipleBirth()) {
            registryPatient.setMultipleBirthOrder(fhirPatient.getMultipleBirthIntegerType().getValue());
        }

        if (fhirPatient.hasId()) {
            registryPatient.setUuid(fhirPatient.getId());
        }

        convertAddresses(fhirPatient, registryPatient);
        convertIdentifiers(fhirPatient, registryPatient);
        convertContacts(fhirPatient, registryPatient);
        convertNames(fhirPatient, registryPatient);

        return registryPatient;
    }

    private static void convertAddresses(Patient fhirPatient, net.ihe.gazelle.app.patientregistryapi.business.Patient registryPatient) throws ConversionException {
        if (fhirPatient.hasAddress()) {
            for (Address address : fhirPatient.getAddress()) {
                registryPatient.addAddress(convertAddress(address));
            }
        }
    }

    private static void convertContacts(Patient fhirPatient, net.ihe.gazelle.app.patientregistryapi.business.Patient registryPatient) throws ConversionException {
        if (fhirPatient.hasContact()) {
            for (ContactComponent contact : fhirPatient.getContact()) {
                registryPatient.addContact(convertContact(contact));
            }
        }
    }

    private static void convertNames(Patient fhirPatient, net.ihe.gazelle.app.patientregistryapi.business.Patient registryPatient) {
        if (fhirPatient.hasName()) {
            for (HumanName name : fhirPatient.getName()) {
                registryPatient.addName(convertName(name));
            }
        }
    }

    private static void convertIdentifiers(Patient fhirPatient, net.ihe.gazelle.app.patientregistryapi.business.Patient registryPatient) {
        if (fhirPatient.hasIdentifier()) {
            for (Identifier id : fhirPatient.getIdentifier()) {
                addEntity(registryPatient, id);
            }
        }
    }

    private static void addEntity(net.ihe.gazelle.app.patientregistryapi.business.Patient registryPatient, Identifier id) {
        EntityIdentifier entityIdentifier = new EntityIdentifier();
        if (id.hasSystem()) {
            String systemID = id.getSystem();
            systemID = systemID.replace(URN_OID, "");
            entityIdentifier.setSystemIdentifier(systemID);
            entityIdentifier.setType(ISO);
        }
        if (id.hasValue()) {
            entityIdentifier.setValue(id.getValue());
        } else {
            throw new InvalidRequestException("Cannot create Patient without any Identifier");
        }
        registryPatient.addIdentifier(entityIdentifier);
    }

    private static GenderCode convertGender(Enumerations.AdministrativeGender gender) throws ConversionException {
        return switch (gender) {
            case MALE -> GenderCode.MALE;
            case FEMALE -> GenderCode.FEMALE;
            case UNKNOWN -> GenderCode.UNDEFINED;
            case OTHER -> GenderCode.OTHER;
            default -> throw new ConversionException(String.format("Cannot map GenderCode : %s", gender));
        };
    }

    private static net.ihe.gazelle.app.patientregistryapi.business.Address convertAddress(Address fhirAddress) throws ConversionException {
        net.ihe.gazelle.app.patientregistryapi.business.Address registryAddress = new net.ihe.gazelle.app.patientregistryapi.business.Address();

        if (fhirAddress.hasCity()) {
            registryAddress.setCity(fhirAddress.getCity());
        }

        if (fhirAddress.hasCountry()) {
            registryAddress.setCountryIso3(fhirAddress.getCountry());
        }

        if (fhirAddress.hasPostalCode()) {
            registryAddress.setPostalCode(fhirAddress.getPostalCode());
        }

        if (fhirAddress.hasState()) {
            registryAddress.setState(fhirAddress.getState());
        }

        if (fhirAddress.hasUse()) {
            registryAddress.setUse(convertAddressUse(fhirAddress.getUse()));
        }

        if (fhirAddress.hasLine()) {
            for (StringType addressLine : fhirAddress.getLine()) {
                registryAddress.addLine(addressLine.getValue());
            }
        }
        return registryAddress;
    }

    private static net.ihe.gazelle.app.patientregistryapi.business.AddressUse convertAddressUse(AddressUse addressUse) throws ConversionException {
        return switch (addressUse) {
            case BILLING -> net.ihe.gazelle.app.patientregistryapi.business.AddressUse.BILLING;
            case HOME -> net.ihe.gazelle.app.patientregistryapi.business.AddressUse.HOME;
            case OLD -> net.ihe.gazelle.app.patientregistryapi.business.AddressUse.BAD;
            case TEMP -> net.ihe.gazelle.app.patientregistryapi.business.AddressUse.TEMPORARY;
            case WORK -> net.ihe.gazelle.app.patientregistryapi.business.AddressUse.WORK;
            default -> throw new ConversionException(String.format("Cannot map address use : %s", addressUse));
        };
    }

    private static Person convertContact(ContactComponent fhirContact) throws ConversionException {
        Person registryContact = new Person();

        if (fhirContact.hasGender()) {
            registryContact.setGender(convertGender(fhirContact.getGender()));
        }

        if (fhirContact.hasAddress()) {
            registryContact.addAddress(convertAddress(fhirContact.getAddress()));
        }

        if (fhirContact.hasName()) {
            registryContact.addName(convertName(fhirContact.getName()));
        }
        return registryContact;
    }

    private static PersonName convertName(HumanName fhirName) {
        PersonName registryName = new PersonName();

        if (fhirName.hasUse()) {
            registryName.setUse(fhirName.getUse().toString());
        }

        if (fhirName.hasFamily()) {
            registryName.setFamily(fhirName.getFamily());
        }

        if (fhirName.hasGiven()) {
            for (StringType givenName : fhirName.getGiven()) {
                registryName.addGiven(givenName.getValue());
            }
        }

        if (fhirName.hasPrefix()) {
            for (StringType prefixName : fhirName.getPrefix()) {
                registryName.setPrefix(prefixName.getValue());
            }
        }

        if (fhirName.hasSuffix()) {
            for (StringType suffixName : fhirName.getSuffix()) {
                registryName.setSuffix(suffixName.getValue());
            }
        }

        return registryName;
    }

    @NotNull
    public static EntityIdentifier convertSourceIdentiferToEntityIdentifier(String sourceIdentifierSystem, String sourceIdentifierValue) {
        if (sourceIdentifierSystem == null) {
            throw new UnprocessableEntityException(SOURCE_IDENTIFIER_PATIENT_IDENTIFIER_NOT_FOUND);
        }
        if (sourceIdentifierValue == null) {
            throw new UnprocessableEntityException(SOURCE_IDENTIFIER_ASSIGNING_AUTHORITY_NOT_FOUND);
        }
        if (sourceIdentifierSystem.isBlank() || sourceIdentifierValue.isBlank()) {
            throw new UnprocessableEntityException(SOURCE_IDENTIFIER_ASSIGNING_AUTHORITY_NOT_FOUND);
        }

        EntityIdentifier wellFormedEntityIdentifier = new EntityIdentifier();
        if (sourceIdentifierSystem.contains(URN_OID)) {
            wellFormedEntityIdentifier.setSystemIdentifier(sourceIdentifierSystem.replace(URN_OID, ""));
            wellFormedEntityIdentifier.setType(ISO);
            wellFormedEntityIdentifier.setValue(sourceIdentifierValue);
        } else if (sourceIdentifierSystem.contains(HTTP_PROTOCOL)) {
            wellFormedEntityIdentifier.setSystemIdentifier(sourceIdentifierSystem);
            wellFormedEntityIdentifier.setType(ISO);
            wellFormedEntityIdentifier.setValue(sourceIdentifierValue);
        } else {
            throw new UnprocessableEntityException(SOURCE_IDENTIFIER_PATIENT_IDENTIFIER_NOT_HTTP_NOR_URN_OID);
        }
        return wellFormedEntityIdentifier;
    }

}
