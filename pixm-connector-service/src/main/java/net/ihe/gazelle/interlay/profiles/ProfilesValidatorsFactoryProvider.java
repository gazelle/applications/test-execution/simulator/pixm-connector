package net.ihe.gazelle.interlay.profiles;

import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import net.ihe.gazelle.application.ProfilesValidatorsFactory;

import java.util.ServiceLoader;

public class ProfilesValidatorsFactoryProvider {

    public static final String NO_VALIDATOR_FOUND_FOR_THIS_PROFIL_ID = "No validator found for this profilID: ";

    public ProfilesValidatorsFactory createProfileValidatorFactory(String profilID) {
        Iterable<ProfilesValidatorsFactory> allProfilesValidators = getListOfAllProfilesValidators();

        for (ProfilesValidatorsFactory profilesValidatorsFactory : allProfilesValidators) {
            if (profilesValidatorsFactory.supports(profilID)) {
                return profilesValidatorsFactory;
            }
        }
        throw new UnprocessableEntityException(NO_VALIDATOR_FOUND_FOR_THIS_PROFIL_ID + profilID);
    }

    private Iterable<ProfilesValidatorsFactory> getListOfAllProfilesValidators() {
        return ServiceLoader.load(ProfilesValidatorsFactory.class);
    }
}
