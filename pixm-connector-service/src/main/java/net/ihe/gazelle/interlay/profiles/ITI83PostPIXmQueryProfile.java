package net.ihe.gazelle.interlay.profiles;

import jakarta.servlet.http.HttpServletRequest;
import net.ihe.gazelle.application.ProfilesValidators;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationReport;
import org.hl7.fhir.r4.model.Resource;

import java.util.ArrayList;
import java.util.List;

public class ITI83PostPIXmQueryProfile implements ProfilesValidators {

    public static final String PIXM_PARAMETERS_PROFILE = System.getenv("PIXM_PARAMETERS_PROFILE");

    @Override
    public List<ValidationReport> validateRequest(HttpServletRequest request, Resource resource, String profileId) {
        List<ValidationReport> reports = new ArrayList<>();
        if (isPostOrPutRequest(request)) {
            reports.add(validateWithFhir(resource, PIXM_PARAMETERS_PROFILE));
        }
        reports.add(validateWithHttpValidator(request, profileId));

        return reports;
    }


}
