package net.ihe.gazelle.interlay.profiles;

import net.ihe.gazelle.application.ProfilesValidators;
import net.ihe.gazelle.application.ProfilesValidatorsFactory;

public class ITI104PatientFeedQueryProfileFactory implements ProfilesValidatorsFactory {

    public static final String PROFILE_ID_CREATE_UPDATE_DELETE_ITI_104 = "PROFILE_ID_CREATE_UPDATE_DELETE_ITI_104";

    @Override
    public ProfilesValidators createProfileValidator() {
        return new ITI104PatientFeedQueryProfile();
    }

    @Override
    public boolean supports(String profileValidatorId) {
        return getProfileId().equals(profileValidatorId);
    }

    @Override
    public String getProfileId() {
        return System.getenv(PROFILE_ID_CREATE_UPDATE_DELETE_ITI_104);
    }
}
