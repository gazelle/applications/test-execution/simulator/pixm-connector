package net.ihe.gazelle.application;

/**
 * The factory has the responsibility of creating the correct formatter for a given document type.
 * The factory is also responsible for determining if it supports a given document type.
 * To learn more about the factory pattern,
 * see <a href="https://refactoring.guru/design-patterns/factory-method/java/example">Factory Method</a>
 */
public interface ProfilesValidatorsFactory {

    /**
     * Creates a formatter for a given document type.
     * The type is already known as each factory only supports one type.
     *
     * @return a formatter
     */
    ProfilesValidators createProfileValidator();

    boolean supports(String profilId);

    String getProfileId();

}
