package net.ihe.gazelle.application;

import ca.uhn.fhir.rest.server.exceptions.ForbiddenOperationException;
import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import com.gitb.ps.ProcessingService;
import net.ihe.gazelle.adapter.preferences.OperationalPreferencesPIXm;
import net.ihe.gazelle.adapter.preferences.Preferences;
import net.ihe.gazelle.app.patientregistryapi.application.SearchCrossReferenceException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.PatientAliases;
import net.ihe.gazelle.app.patientregistryxrefsearchclient.adapter.XRefSearchClient;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.framework.operationalpreferencesservice.adapter.JNDIPropertiesLoader;
import net.ihe.gazelle.framework.operationalpreferencesservice.application.PreferencesServiceFactory;
import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesClientApplication;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;
import net.ihe.gazelle.lib.annotations.Package;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.hl7.fhir.r4.model.Parameters;
import org.hl7.fhir.r4.model.Reference;

import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.xml.ws.WebServiceException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;

@Named("patientRegistryXRefSearchClient")
public class PatientRegistryXRefSearchClient {
    public static final String ERROR_IN_THE_SOURCE_IDENTIFIER_SYSTEM_DOES_NOT_EXIT = "Error in the sourceIdentifier : System does not exit";
    public static final String THE_SYSTEM_IDENTIFIER_FROM_SOURCE_IDENTIFIER_CANNOT_BE_NULL_OR_EMPTY = "The system identifier from sourceIdentifier " +
            "cannot be null or empty";
    public static final String ERROR_IN_THE_SOURCE_IDENTIFIER_IT_DOES_NOT_MATCH_ANY_PATIENT = "Error in the sourceIdentifier : it does not match " +
            "any Patient";
    public static final String ERROR_IN_THE_SOURCE_IDENTIFIER_IT_DOES_NOT_MATCH_ANY_IDENTITY = "Error in the sourceIdentifier : it does not match " +
            "any identity";
    public static final String ONE_OF_THE_TARGET_DOMAIN_DOES_NOT_EXIST = "One of the target domain does not exist";
    public static final String SOURCE_IDENTIFIER_ASSIGNING_AUTHORITY_NOT_FOUND = "sourceIdentifier Assigning Authority not found";
    public static final String SOURCE_IDENTIFIER_PATIENT_IDENTIFIER_NOT_FOUND = "sourceIdentifier Patient Identifier not found";
    public static final String TARGET_SYSTEM_NOT_FOUND = "targetSystem not found";
    private static final GazelleLogger logger = GazelleLoggerFactory.getInstance().getLogger(PatientRegistryXRefSearchClient.class);
    private String serverUrl;
    private OperationalPreferencesService operationalPreferencesService;
    private XRefSearchClient client;

    /**
     * Default constructor, used for injection.
     *
     * @param operationalPreferencesService {@link OperationalPreferencesService} used to retrieve Patient Registry's URL.
     */
    @Inject
    public PatientRegistryXRefSearchClient(OperationalPreferencesService operationalPreferencesService) {

        this.operationalPreferencesService = operationalPreferencesService;
    }

    public PatientRegistryXRefSearchClient() {

    }

    /**
     * Package-private constructor used for test purposes.
     *
     * @param url {@link URL} to be used by this to instantiate the processing service to retrieve patients.
     */
    @Package
    PatientRegistryXRefSearchClient(URL url) {
        this.client = new XRefSearchClient(url);
    }


    /**
     * Package-private constructor used for test purposes.
     *
     * @param service {@link ProcessingService} to be used by this to retrieve Patients.
     */
    @Package
    PatientRegistryXRefSearchClient(ProcessingService service) {
        this.client = new XRefSearchClient(service);
    }

    @Package
    public void setClient(XRefSearchClient client) {
        this.client = client;
    }

    /**
     * Initialize the Search Client using the Operational Preferences Service.
     *
     * @throws SearchCrossReferenceException if the service cannot be instantiated.
     */
    private void initializeSearchClient() throws SearchCrossReferenceException {
        if (client == null) {
            String patientRegistryUrl = null;
            try {
                PreferencesServiceFactory preferencesServiceFactory = new PreferencesServiceFactory();
                OperationalPreferencesService operationalPreferencesService = preferencesServiceFactory.createOperationalPreferencesService(new JNDIPropertiesLoader(), getPrefForMyApp());

                patientRegistryUrl = operationalPreferencesService.
                        getStringValue(Preferences.XREF_PATREG.getNamespace().getValue(),
                                Preferences.XREF_PATREG.getKey());
                this.client = new XRefSearchClient(new URL(patientRegistryUrl));
            } catch (NamespaceException | PreferenceException e) {
                throw new SearchCrossReferenceException(String.format("Unable to retrieve [%s] Preference !",
                        Preferences.XREF_PATREG.getKey()));
            } catch (MalformedURLException e) {
                throw new SearchCrossReferenceException(String.format("Preference [%s] with value [%s] is not a valid URL !",
                        Preferences.XREF_PATREG.getKey(),
                        patientRegistryUrl));
            } catch (WebServiceException e) {
                throw new SearchCrossReferenceException("Can't connect to patient registry !");
            }
        }
        if (serverUrl == null) {
            getUrlOfServer();
        }

    }

    OperationalPreferencesClientApplication getPrefForMyApp(){
        return new OperationalPreferencesPIXm();
    }


    /**
     * Proceed to the request using the XReferencePatientRegistry client and  prepare the fhir response
     *
     * @param sourceIdentifier Identity of the patient
     * @param targetSystemList targetSystem we want to retrieve through PatientRegistry
     * @return Fhir response that contain the target identity
     * @throws SearchCrossReferenceException if the client can not be initialized
     */
    public Parameters process(EntityIdentifier sourceIdentifier, List<String> targetSystemList) throws SearchCrossReferenceException {
        initializeSearchClient();
        PatientAliases patientAliases;
        try {
            patientAliases = client.search(sourceIdentifier, targetSystemList);
        } catch (SearchCrossReferenceException searchCrossReferenceException) {
            switch (searchCrossReferenceException.getCause().getMessage()) {
                case ERROR_IN_THE_SOURCE_IDENTIFIER_SYSTEM_DOES_NOT_EXIT, THE_SYSTEM_IDENTIFIER_FROM_SOURCE_IDENTIFIER_CANNOT_BE_NULL_OR_EMPTY ->
                        throw new InvalidRequestException(SOURCE_IDENTIFIER_ASSIGNING_AUTHORITY_NOT_FOUND, generateOperationOutcome("code-invalid",
                                SOURCE_IDENTIFIER_ASSIGNING_AUTHORITY_NOT_FOUND));
                case ERROR_IN_THE_SOURCE_IDENTIFIER_IT_DOES_NOT_MATCH_ANY_PATIENT, ERROR_IN_THE_SOURCE_IDENTIFIER_IT_DOES_NOT_MATCH_ANY_IDENTITY ->
                        throw new ResourceNotFoundException(SOURCE_IDENTIFIER_PATIENT_IDENTIFIER_NOT_FOUND, generateOperationOutcome("not-found",
                                SOURCE_IDENTIFIER_PATIENT_IDENTIFIER_NOT_FOUND));
                case ONE_OF_THE_TARGET_DOMAIN_DOES_NOT_EXIST ->
                        throw new ForbiddenOperationException(TARGET_SYSTEM_NOT_FOUND, generateOperationOutcome("code-invalid", TARGET_SYSTEM_NOT_FOUND));
                default ->
                        throw new InternalErrorException("An internal error occurred", searchCrossReferenceException);
            }
        }

        Parameters parameters = new Parameters();
        if (patientAliases.getMembers().isEmpty()) {
            throw new ResourceNotFoundException("No crossReferenced Patient found.");
        }

        logger.info("Number of patents found : " + patientAliases.getMembers().size());

        for (net.ihe.gazelle.app.patientregistryapi.business.Patient pat : patientAliases.getMembers()) {

            for (EntityIdentifier entity : pat.getIdentifiers()) {
                if (addMatchingIdentifierWithTargetSystem(targetSystemList, entity.getSystemIdentifier())) {
                    Identifier r4Identifier = new Identifier();
                    r4Identifier.setSystem(entity.getSystemIdentifier());
                    r4Identifier.setValue(entity.getValue());

                    Parameters.ParametersParameterComponent alias = new Parameters.ParametersParameterComponent();
                    alias.setName("target Identifier");
                    alias.setValue(r4Identifier);
                    parameters.addParameter(alias);
                }

            }
            Parameters.ParametersParameterComponent targetId = new Parameters.ParametersParameterComponent();
            targetId.setName("targetId");
            Reference ref = new Reference();
            ref.setReference("https://" + serverUrl + "/pixm-connector/fhir/ihe/Patient/" + pat.getUuid());
            targetId.setValue(ref);

            parameters.addParameter(targetId);
        }
        return parameters;


    }

    /**
     * Method to construct the fhir response, return true if the entityIdentifier match a targetSystem and false otherwise
     * It returns true is the targetSystem list is empty.
     *
     * @param targetSystemList
     * @param entityIdentifierSystem
     * @return boolean
     */
    private boolean addMatchingIdentifierWithTargetSystem(List<String> targetSystemList, String entityIdentifierSystem) {
        if (targetSystemList.isEmpty()) {
            return true;
        }

        for (String system : targetSystemList) {
            if (system.equals(entityIdentifierSystem)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Method to generate the Error outcome in the response
     *
     * @param codeString  Code of the error catch
     * @param diagnostics origin of the issue
     * @return generated Operation outcome for the Fhir response
     */
    private OperationOutcome generateOperationOutcome(String codeString, String diagnostics) {
        OperationOutcome code = new OperationOutcome();
        OperationOutcome.OperationOutcomeIssueComponent issue = new OperationOutcome.OperationOutcomeIssueComponent();
        issue.setSeverity(OperationOutcome.IssueSeverity.ERROR);
        issue.setCode(OperationOutcome.IssueType.fromCode(codeString));
        issue.setDiagnostics(diagnostics);
        code.addIssue(issue);
        return code;
    }

    /**
     * The method is used to retrieve the url of the current server.
     */
    private void getUrlOfServer() {
        InetAddress ip;
        try {
            ip = InetAddress.getLocalHost();
            serverUrl = ip.getCanonicalHostName();
            logger.info(String.format("Your current Hostname : %s", serverUrl));

        } catch (UnknownHostException exception) {
            logger.error("Unable to find serverUrl");
            serverUrl = "ReplaceByTheRightEndpoint";
        }
    }

}