package net.ihe.gazelle.application;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import jakarta.servlet.http.HttpServletRequest;
import net.ihe.gazelle.http.validator.client.interlay.validation.HttpValidationServiceFactory;
import net.ihe.gazelle.matchbox.client.interlay.validation.CustomPatientServiceFactory;
import net.ihe.gazelle.validation.api.application.ValidationService;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationReport;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationSubReport;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationTestResult;
import net.ihe.gazelle.validation.api.domain.request.structure.ValidationItem;
import net.ihe.gazelle.validation.api.domain.request.structure.ValidationRequest;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Resource;
import org.hl7.fhir.r4.model.codesystems.HttpVerb;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public interface ProfilesValidators {

    String CRLF = "\r\n";
    String PROBLEM_DURING_VALIDATION = "Problem during Validation of Pixm Patient: ";

    List<ValidationReport> validateRequest(HttpServletRequest request, Resource resource, String profileId);


    default ValidationReport validateWithHttpValidator(HttpServletRequest request, String profileId) {
        String messageToValidate = getFullHttpMessageFromRequest(request);
        ValidationRequest validationRequest = constructValidationRequest(messageToValidate, profileId);
        validationRequest.setValidationServiceName("HTTP Validator");
        ValidationService patientValidationService = new HttpValidationServiceFactory().getValidationService();
        return patientValidationService.validate(validationRequest);
    }

    default ValidationRequest constructValidationRequest(Object obj, String profileId) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ValidationRequest validationRequest = new ValidationRequest();
        validationRequest.setValidationProfileId(profileId);
        byte[] data;

        if (obj instanceof Resource) {
            String resourceToString = FhirContext.forR4().newJsonParser().encodeResourceToString((IBaseResource) obj);
            data = resourceToString.getBytes(StandardCharsets.UTF_8);
        } else if (obj instanceof String) {
            data = ((String) obj).getBytes(StandardCharsets.UTF_8);
        } else {
            try (ObjectOutputStream outputStream = new ObjectOutputStream(baos)) {
                outputStream.writeObject(obj);
                data = baos.toByteArray();
            } catch (IOException e) {
                throw new UnprocessableEntityException(PROBLEM_DURING_VALIDATION, e);
            }
        }
        validationRequest.setValidationItems(List.of(new ValidationItem().setContent(data)));
        return validationRequest;
    }

    default String getFullHttpMessageFromRequest(HttpServletRequest request) {
        String url = (request != null)
                ? getFullURL(request)
                : "";
        String httpMessage = (request != null)
                ? formatHttpMessageIntoOneString(request)
                : "";

        StringBuilder sb = new StringBuilder(url);
        sb.append(CRLF)
                .append(httpMessage)
                .append(CRLF);
        return sb.toString();
    }

    default String getFullURL(HttpServletRequest request) {
        StringBuilder sb;
        sb = new StringBuilder();
        sb.append(request.getMethod());
        sb.append(" ");
        sb.append(request.getRequestURL());
        if(null != request.getQueryString()){
            sb.append("?");
            sb.append(request.getQueryString());
        }
        sb.append(" ");
        sb.append(request.getProtocol());
        return sb.toString();
    }

    default Map<String, String> getAllHeaders(HttpServletRequest request) {
        return Collections.list(request.getHeaderNames())
                .stream()
                .collect(Collectors.toMap(h -> h, request::getHeader));
    }

    default String formatHttpMessageIntoOneString(HttpServletRequest request) {
        Map<String, String> headers = getAllHeaders(request);
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> header : headers.entrySet()) {
            sb.append(header.getKey())
                    .append(": ")
                    .append(header.getValue())
                    .append(CRLF);
        }
        return sb.toString();
    }


    default boolean isPostOrPutRequest(HttpServletRequest request) {
        return HttpVerb.POST.toCode().equals(request.getMethod()) || HttpVerb.PUT.toCode().equals(request.getMethod());
    }

    default ValidationReport validateWithFhir(Resource resource, String profileId) {
        ValidationRequest validationRequest;
        validationRequest = constructValidationRequest(resource, profileId);
        ValidationService patientValidationService = new CustomPatientServiceFactory().getValidationService();
        return patientValidationService.validate(validationRequest);
    }

}
