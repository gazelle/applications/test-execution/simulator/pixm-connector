package net.ihe.gazelle.application;

import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.ResourceGoneException;
import jakarta.inject.Named;
import jakarta.xml.ws.WebServiceException;
import net.ihe.gazelle.adapter.connector.ConversionException;
import net.ihe.gazelle.adapter.connector.FhirToGazelleRegistryConverter;
import net.ihe.gazelle.adapter.connector.GazelleRegistryToFhirConverter;
import net.ihe.gazelle.adapter.preferences.OperationalPreferencesPIXm;
import net.ihe.gazelle.adapter.preferences.Preferences;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import net.ihe.gazelle.app.patientregistryfeedclient.adapter.PatientFeedClient;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.framework.operationalpreferencesservice.adapter.JNDIPropertiesLoader;
import net.ihe.gazelle.framework.operationalpreferencesservice.application.PreferencesServiceFactory;
import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesClientApplication;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;
import net.ihe.gazelle.lib.annotations.Package;
import org.hl7.fhir.r4.model.Bundle;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Patient;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;

@Named("PatientRegistryFeedClient")
public class PatientRegistryFeedClient {
    public static final String NO_UUID = "No UUID was retrieved from the created Patient";
    public static final String NO_PATIENT_PARAMETER = "No Patient were given to create";
    public static final String CLIENT_NOT_SET = "client not set";
    public static final String INVALID_PARAMETERS = "Invalid parameters";
    public static final String MANDATORY_FIELD_ARE_MISSING = "Mandatory fields are missing: family name, Date of Birth or Gender";
    private static final String UUID = "PatientPIXmFeed";
    private static final GazelleLogger logger = GazelleLoggerFactory.getInstance().getLogger(PatientRegistryFeedClient.class);
    public static final String UUID_CANNOT_BE_NULL_OR_EMPTY = "The uuid cannot be null or empty";
    public static final String CANNOT_PROCEED_TO_DELETE = "Cannot proceed to delete";
    private String serverUrl;
    private OperationalPreferencesService operationalPreferencesService;
    private PatientFeedClient client;

    public PatientRegistryFeedClient() {
    }

    /**
     * Default constructor, used for injection.
     *
     * @param operationalPreferencesService {@link OperationalPreferencesService} used to retrieve Patient Registry's URL.
     */
    @Package
    public PatientRegistryFeedClient(OperationalPreferencesService operationalPreferencesService) {

        this.operationalPreferencesService = operationalPreferencesService;
    }

    /**
     * Package-private constructor used for test purposes.
     *
     * @param url {@link URL} to be used by this to instantiate the processing service to retrieve patients.
     */
    @Package
    PatientRegistryFeedClient(URL url) {
        this.client = new PatientFeedClient(url);
    }

    private static boolean areMandatoryFieldsPresent(net.ihe.gazelle.app.patientregistryapi.business.Patient patient) {
        PersonName name = patient.getNames().get(0);
        return name.getFamily().isBlank()
                || name.getGivens().isEmpty()
                || patient.getDateOfBirth() == null
                || patient.getGender() == null;
    }

    private static boolean isNullOrBlank(String uuid) {
        return uuid == null || uuid.isBlank();
    }

    @Package
    public void setClient(PatientFeedClient client) {
        this.client = client;
    }

    /**
     * Initialize the Search Client using the Operational Preferences Service.
     *
     * @throws PatientFeedException if the service cannot be instantiated.
     */
    private void initializeClient() throws PatientFeedException {



        String patientRegistryUrl = null;
        getUrlOfServer();
        try {
            PreferencesServiceFactory preferencesServiceFactory = new PreferencesServiceFactory();
            OperationalPreferencesService preferencesService = preferencesServiceFactory.createOperationalPreferencesService(new JNDIPropertiesLoader(), getPrefForMyApp());

            String patRegNamespaceValue = Preferences.PATIENT_REGISTRY_URL.getNamespace().getValue();
            String patRegUrlKey = Preferences.PATIENT_REGISTRY_URL.getKey();
            patientRegistryUrl = preferencesService.getStringValue(patRegNamespaceValue,patRegUrlKey);
            this.client = new PatientFeedClient(new URL(patientRegistryUrl));
        } catch (NamespaceException | PreferenceException e) {
            throw new PatientFeedException(String.format("Unable to retrieve [%s] Preference !", Preferences.PATIENT_REGISTRY_URL.getKey()));
        } catch (MalformedURLException e) {
            throw new PatientFeedException(String.format("Preference [%s] with value [%s] is not a valid URL !",
                    Preferences.PATIENT_REGISTRY_URL.getKey(),
                    patientRegistryUrl));
        } catch (WebServiceException e) {
            logger.warn(e.getMessage());
            throw new PatientFeedException(String.format("Can't connect to patient registry ! at address [%s]", patientRegistryUrl));
        }
    }

    OperationalPreferencesClientApplication getPrefForMyApp(){
        return new OperationalPreferencesPIXm();
    }

    /**
     * Method called to create a Patient in the Patient Registry Database
     *
     * @param patient : the patient to add in database, represented in patientRegistry model
     * @return a string corresponding to the uuid of the newly created patient in the database.
     */
    public Patient createPatient(net.ihe.gazelle.app.patientregistryapi.business.Patient patient) throws PatientFeedException, ConversionException {
        if (client == null) {
            logger.info(CLIENT_NOT_SET);
            initializeClient();
        }
        if (patient == null) {
            throw new InvalidRequestException(NO_PATIENT_PARAMETER);
        }

        if (areMandatoryFieldsPresent(patient)) {
            throw new InvalidRequestException(MANDATORY_FIELD_ARE_MISSING);
        }
        String uuid;
        try {
            patient.setUuid(UUID);
            patient.setActive(true);
            uuid = client.createPatient(patient);
            patient.setUuid(uuid);

            if (uuid.isBlank()) {
                throw new InternalErrorException(NO_UUID);
            }

        } catch (PatientFeedException e) {
            switch (e.getCause().getMessage()) {
                case "Impossible to cross reference the patient (not saved)" ->
                        throw new InternalErrorException("Impossible to cross reference the patient, it will not be saved !", e);
                case "Unexpected Exception persisting Patient !" ->
                        throw new InternalErrorException("Unexpected Exception persisting Patient !", e);
                default -> treatClientBaseErrors(e);
            }
        }
        return GazelleRegistryToFhirConverter.convertPatient(patient);
    }

    /**
     * Method called to update a Patient in the Patient Registry Database.
     *
     * @param patient : the Patient object we want to insert in DB
     * @return a String corresponding to the uuid confirming the transaction has been successful.
     */
    public Patient updatePatient(net.ihe.gazelle.app.patientregistryapi.business.Patient patient, EntityIdentifier identifier) throws PatientFeedException, ConversionException {
        if (client == null) {
            logger.info(CLIENT_NOT_SET);
            initializeClient();
        }
        if (patient == null ) {
            throw new InvalidRequestException(INVALID_PARAMETERS);
        }
        patient.setUuid(UUID);

        Patient updatedPatient = null;
        try {
            updatedPatient = GazelleRegistryToFhirConverter.convertPatient(client.updatePatient(patient, identifier));
        } catch (PatientFeedException e) {
            treatClientBaseErrors(e);
        }
        return updatedPatient;


    }

    /**
     * Method called to merge two Patients in the Patient Registry Database.
     *
     * @param uuidOriginal   : the Patient object we want to insert in DB
     * @param uuidDuplicated : The uuid of the patient corresponding to it in DB
     * @return a String corresponding to the uuid confirming the transaction has been successful.
     */
    //TODO: Implement response for merge
    public Bundle mergePatient(String uuidOriginal, String uuidDuplicated) throws PatientFeedException {
        if (client == null) {
            logger.info(CLIENT_NOT_SET);
            initializeClient();
        }
        if (isNullOrBlank(uuidOriginal) || isNullOrBlank(uuidDuplicated)) {
            throw new InvalidRequestException(INVALID_PARAMETERS);
        }

        Bundle response = new Bundle();
        try {

            response.setId(client.mergePatient(uuidOriginal, uuidDuplicated));

        } catch (PatientFeedException e) {
            treatClientBaseErrors(e);
        }
        return response;
    }

    /**
     * Deactivate a patient in the Patient Registry Database by adding all linked identifiers to the patient.
     * @param itiPatient the patient to deactivate
     * @return the deactivated patient
     * @throws PatientFeedException If deactivation cannot be performed.
     */
    public Patient deactivatePatient(Patient itiPatient, EntityIdentifier identifier) throws PatientFeedException {
        if(itiPatient.getActive()){
                throw new InvalidRequestException("Patient not intended to be deactivated");
        }
        List<Identifier> identifiersToAdd = itiPatient
                .getLink()
                .stream()
                .map(elm -> elm.getOther().getIdentifier())
                .toList();
        itiPatient.getIdentifier().addAll(identifiersToAdd);
        try {
            return this.updatePatient(FhirToGazelleRegistryConverter.convertPatient(itiPatient), identifier);
        } catch (PatientFeedException | ConversionException e) {
            throw new PatientFeedException("Unable to deactivate patient", e);
        }
    }

    /***
     *
     * @param identifier
     * @return
     * @throws PatientFeedException
     */
    public boolean delete(String identifier) throws PatientFeedException {
        boolean deletionStatus = Boolean.FALSE;
        if (client == null) {
            logger.info(CLIENT_NOT_SET);
            initializeClient();
        }
        if (identifier.isBlank()) {
            throw new InvalidRequestException("Invalid parameter");
        }
        try {
            deletionStatus = client.deletePatient(identifier);
            if (!deletionStatus) {
                throw new ResourceGoneException("Patient with UUID " + identifier);
            }
        } catch (PatientFeedException exception) {
            switch (exception.getCause().getMessage()) {
                case UUID_CANNOT_BE_NULL_OR_EMPTY ->
                        throw new InternalErrorException(UUID_CANNOT_BE_NULL_OR_EMPTY, exception);
                case CANNOT_PROCEED_TO_DELETE ->
                        throw new InternalErrorException(CANNOT_PROCEED_TO_DELETE, exception);
                default -> treatClientBaseErrors(exception);
            }
        }
        return deletionStatus;
    }

    public EntityIdentifier delete(EntityIdentifier identifier) throws PatientFeedException {
        EntityIdentifier deleted = null;
        if (client == null) {
            logger.info(CLIENT_NOT_SET);
            initializeClient();
        }
        if (identifier == null || identifier.getValue() == null) {
            throw new InvalidRequestException("Invalid identifier");
        }
        try {
            deleted = client.deletePatient(identifier);
            if (deleted == null) {
                throw new ResourceGoneException("Cannot delete patient with identifier " + identifier);
            }
        } catch (PatientFeedException exception) {
            switch (exception.getCause().getMessage()) {
                case UUID_CANNOT_BE_NULL_OR_EMPTY ->
                        throw new InternalErrorException(UUID_CANNOT_BE_NULL_OR_EMPTY, exception);
                case CANNOT_PROCEED_TO_DELETE ->
                        throw new InternalErrorException(CANNOT_PROCEED_TO_DELETE, exception);
                default -> treatClientBaseErrors(exception);
            }
        }
        return deleted;
    }

    /**
     * The method is used to retrieve the url of the current server.
     */
    private void getUrlOfServer() {
        InetAddress ip;
        try {
            ip = InetAddress.getLocalHost();
            serverUrl = ip.getCanonicalHostName();
            logger.info(String.format("Your current Hostname : %s", serverUrl));

        } catch (UnknownHostException exception) {
            logger.error("Unable to find serverUrl");
            serverUrl = "ReplaceByTheRightEndpoint";
        }
    }

    /**
     * Private Method which goal is to process the first levels exceptions thrown by the Patient feed client.
     *
     * @param e the exception thrown
     * @throws InternalErrorException throws back the exception with the right http code and the stack trace.
     */
    private void treatClientBaseErrors(Exception e) {
        switch (e.getMessage()) {
            case "Exception while Mapping with GITB elements !" ->
                    throw new InternalErrorException("Exception while Mapping with GITB elements !", e);
            case "Invalid Response from distant PatientFeedProcessingService !" ->
                    throw new InternalErrorException("Invalid Response from distant PatientFeedProcessingService !", e);
            case "Invalid operation used on distant PatientFeedProcessingService !" ->
                    throw new InternalErrorException("Invalid operation used on distant PatientFeedProcessingService !", e);
            case "Invalid Request sent to distant PatientFeedProcessingService !" ->
                    throw new InternalErrorException("Invalid Request sent to distant PatientFeedProcessingService !", e);
            default -> throw new InternalErrorException("An unhandled error was thrown.", e);
        }
    }

}
