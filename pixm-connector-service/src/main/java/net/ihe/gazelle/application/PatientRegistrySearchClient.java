package net.ihe.gazelle.application;

import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import com.gitb.ps.ProcessingService;
import net.ihe.gazelle.adapter.connector.ConversionException;
import net.ihe.gazelle.adapter.connector.GazelleRegistryToFhirConverter;
import net.ihe.gazelle.adapter.preferences.OperationalPreferencesPIXm;
import net.ihe.gazelle.adapter.preferences.Preferences;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PatientSearchCriterionKey;
import net.ihe.gazelle.app.patientregistrysearchclient.adapter.PatientSearchClient;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLogger;
import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.framework.operationalpreferencesservice.adapter.JNDIPropertiesLoader;
import net.ihe.gazelle.framework.operationalpreferencesservice.application.PreferencesServiceFactory;
import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesClientApplication;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;
import net.ihe.gazelle.lib.annotations.Package;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.SearchCriterion;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.StringSearchCriterion;
import org.hl7.fhir.r4.model.OperationOutcome;

import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.xml.ws.WebServiceException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Named("patientRegistrySearchClient")
public class PatientRegistrySearchClient {

    public static final String PROCESSING_UUID_OF_PATIENT = "Processing uuid of Patient";
    public static final String NOT_FOUND = "not-found";
    private static final String INVALID_REQUEST = "Invalid Request sent to distant PatientFeedProcessingService !";
    private static final String INVALID_OPERATION = "Invalid operation used on distant PatientFeedProcessingService !";
    private static final String INVALID_RESPONSE = "Invalid Response from distant PatientFeedProcessingService !";
    private static final String INVALID_MAPPING = "Exception while Mapping with GITB elements !";
    private static final String CONVERSION_ERROR = "A problem occured when Converting Patient from Patient Registry model to hl7 v4";
    private static final String NO_UUID = "No patient Uuid given in parameter";
    private static final String NO_PATIENT_FOUND = "No Patient were found";
    private static final String MORE_THAN_ONE_PATIENT_FOUND = "More than one Patient was found for this ID";

    private static final GazelleLogger logger = GazelleLoggerFactory.getInstance().getLogger(PatientRegistrySearchClient.class);
    private OperationalPreferencesService operationalPreferencesService;
    private PatientSearchClient client;

    /**
     * Default constructor, used for injection.
     *
     * @param operationalPreferencesService {@link OperationalPreferencesService} used to retrieve Patient Registry's URL.
     */
    @Inject
    public PatientRegistrySearchClient(OperationalPreferencesService operationalPreferencesService) {

        this.operationalPreferencesService = operationalPreferencesService;
    }

    public PatientRegistrySearchClient() {

    }

    /**
     * Package-private constructor used for test purposes.
     *
     * @param url {@link URL} to be used by this to instantiate the processing service to retrieve patients.
     */
    @Package
    PatientRegistrySearchClient(URL url) {
        this.client = new PatientSearchClient(url);
    }


    /**
     * Package-private constructor used for test purposes.
     *
     * @param service {@link ProcessingService} to be used by this to retrieve Patients.
     */
    @Package
    PatientRegistrySearchClient(ProcessingService service) {
        this.client = new PatientSearchClient(service);
    }

    @Package
    public void setClient(PatientSearchClient client) {
        this.client = client;
    }

    /**
     * Method To call the Search Client from Patient Registry with the uuid of a Patient.
     *
     * @param patientUuid the uuid of the Patient as a String
     * @return a single Patient (in hl7 v4 model) corresponding to the uuid given
     * @throws SearchException
     */
    public org.hl7.fhir.r4.model.Patient searchPatient(String patientUuid) throws SearchException {
        if (patientUuid == null || patientUuid.isEmpty()) {
            logger.error(NO_UUID);
            throw new InvalidRequestException(NO_UUID);
        }

        if (client == null) {
            logger.info("client not set");
            initializeSearchClient();
        }

        logger.info(PROCESSING_UUID_OF_PATIENT);


        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion<String> searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(patientUuid);
        searchCriteria.addSearchCriterion(searchCriterion);

        List<org.hl7.fhir.r4.model.Patient> resources = new ArrayList<>();

        try {

            logger.info("Sending request to patient registry");
            List<Patient> patients = client.search(searchCriteria);

            if (patients.isEmpty()) {
                logger.error("No Patient found");
                throw new ResourceNotFoundException(NO_PATIENT_FOUND, generateOperationOutcome(NOT_FOUND, NO_PATIENT_FOUND));
            }

            if (patients.size() > 1) {
                logger.error("More than one Patient were found");
                throw new ResourceNotFoundException(MORE_THAN_ONE_PATIENT_FOUND, generateOperationOutcome(NOT_FOUND, MORE_THAN_ONE_PATIENT_FOUND));
            }

            logger.info("Search complete");
            for (Patient pat : patients) {
                logger.info("converting patient to right model");
                resources.add(GazelleRegistryToFhirConverter.convertPatient(pat));
            }
        } catch (ConversionException e) {
            logger.error(CONVERSION_ERROR);
            throw new InternalErrorException(CONVERSION_ERROR, e);
        } catch (SearchException e) {
            logger.warn(e.getMessage());
            switch (e.getMessage()) {
                case INVALID_REQUEST:
                    throw new InvalidRequestException(INVALID_REQUEST, generateOperationOutcome("code-invalid", INVALID_REQUEST));
                case INVALID_OPERATION:
                    throw new InvalidRequestException(INVALID_OPERATION, generateOperationOutcome("code-invalid", INVALID_OPERATION));
                case INVALID_RESPONSE:
                    throw new ResourceNotFoundException(INVALID_RESPONSE, generateOperationOutcome(NOT_FOUND, INVALID_RESPONSE));
                case INVALID_MAPPING:
                    throw new InternalErrorException(INVALID_MAPPING);
                default:
                    throw new InternalErrorException(e);
            }
        }


        return resources.get(0);
    }

    /**
     * Initialize the Search Client using the Operational Preferences Service.
     *
     * @throws SearchException if the service cannot be instantiated.
     */
    private void initializeSearchClient() throws SearchException {
        String patientRegistryUrl = null;
        try {
            PreferencesServiceFactory preferencesServiceFactory = new PreferencesServiceFactory();
            OperationalPreferencesService operationalPreferencesService = preferencesServiceFactory.createOperationalPreferencesService(new JNDIPropertiesLoader(), getPrefForMyApp());

            patientRegistryUrl = operationalPreferencesService.
                    getStringValue(Preferences.PATIENT_REGISTRY_URL.getNamespace().getValue(), Preferences.PATIENT_REGISTRY_URL.getKey());
            this.client = new PatientSearchClient(new URL(patientRegistryUrl));
        } catch (NamespaceException | PreferenceException e) {
            throw new SearchException(String.format("Unable to retrieve [%s] Preference !", Preferences.PATIENT_REGISTRY_URL.getKey()));
        } catch (MalformedURLException e) {
            throw new SearchException(String.format("Preference [%s] with value [%s] is not a valid URL !", Preferences.PATIENT_REGISTRY_URL.getKey(),
                    patientRegistryUrl));
        } catch (WebServiceException e) {
            logger.warn(e.getMessage());
            throw new SearchException(String.format("Can't connect to patient registry ! at address [%s]", patientRegistryUrl));
        }

    }

    OperationalPreferencesClientApplication getPrefForMyApp(){
        return new OperationalPreferencesPIXm();
    }



    /**
     * Method to generate the Error outcome in the response
     *
     * @param codeString  Code of the error catch
     * @param diagnostics origin of the issue
     * @return generated Operation outcome for the Fhir response
     */
    private OperationOutcome generateOperationOutcome(String codeString, String diagnostics) {
        OperationOutcome code = new OperationOutcome();
        OperationOutcome.OperationOutcomeIssueComponent issue = new OperationOutcome.OperationOutcomeIssueComponent();
        issue.setSeverity(OperationOutcome.IssueSeverity.ERROR);
        issue.setCode(OperationOutcome.IssueType.fromCode(codeString));
        issue.setDiagnostics(diagnostics);
        code.addIssue(issue);
        return code;
    }
}
