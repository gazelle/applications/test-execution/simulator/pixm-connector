package net.ihe.gazelle.business.interceptor;

import ca.uhn.fhir.interceptor.api.Hook;
import ca.uhn.fhir.interceptor.api.Pointcut;
import ca.uhn.fhir.rest.api.server.RequestDetails;
import ca.uhn.fhir.rest.server.exceptions.BaseServerResponseException;
import ca.uhn.fhir.rest.server.interceptor.InterceptorAdapter;
import ca.uhn.fhir.rest.server.servlet.ServletRequestDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LegacyLoggingInterceptor extends InterceptorAdapter {
    private static final Logger ourLog = LoggerFactory.getLogger(LegacyLoggingInterceptor.class);

    public boolean incomingRequestPostProcessed(RequestDetails theRequest,
                                                HttpServletRequest theSrvRequest,
                                                HttpServletResponse theSrvResponse) {

        ourLog.info("Handling {} client operation on ID {}", theRequest.getRequestType(), theRequest.getId());

        return true; // Processing should continue
    }

    @Hook(Pointcut.SERVER_HANDLE_EXCEPTION)
    public boolean handleException(
            RequestDetails theRequestDetails,
            BaseServerResponseException theException,
            HttpServletRequest theServletRequest,
            HttpServletResponse theServletResponse) throws IOException {

        // HAPI's server exceptions know what the appropriate HTTP status code is
        theServletResponse.setStatus(theException.getStatusCode());

        // Provide a response ourself
        theServletResponse.setContentType("text/plain");
        theServletResponse.getWriter().append("Failed to process!");
        theServletResponse.getWriter().close();

        // Since we handled this response in the interceptor, we must return false
        // to stop processing immediately
        return false;
    }

    @Override
    @Hook(Pointcut.SERVER_OUTGOING_RESPONSE)
    public boolean outgoingResponse(RequestDetails theRequestDetails) {
        ServletRequestDetails details = (ServletRequestDetails) theRequestDetails;
        return outgoingResponse(theRequestDetails, details.getServletRequest(), details.getServletResponse());
    }
}
