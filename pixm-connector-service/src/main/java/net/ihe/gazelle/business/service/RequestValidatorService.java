package net.ihe.gazelle.business.service;

import jakarta.servlet.http.HttpServletRequest;
import net.ihe.gazelle.application.ProfilesValidators;
import net.ihe.gazelle.application.ProfilesValidatorsFactory;
import net.ihe.gazelle.interlay.profiles.ProfilesValidatorsFactoryProvider;
import net.ihe.gazelle.validation.api.domain.report.structure.ValidationReport;
import org.hl7.fhir.r4.model.Resource;

import java.util.List;

public class RequestValidatorService {

    private final ProfilesValidatorsFactoryProvider profilesValidatorsFactoryProvider;

    public RequestValidatorService() {
        this.profilesValidatorsFactoryProvider = new ProfilesValidatorsFactoryProvider();
    }

    public List<ValidationReport> validateRequest(HttpServletRequest request, Resource resource, String profileId) {
        ProfilesValidatorsFactory profilesValidatorsFactory = profilesValidatorsFactoryProvider.createProfileValidatorFactory(profileId);
        ProfilesValidators validators = profilesValidatorsFactory.createProfileValidator();
        return validators.validateRequest(request, resource, profileId);
    }


}
