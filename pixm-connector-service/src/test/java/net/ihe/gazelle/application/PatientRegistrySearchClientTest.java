package net.ihe.gazelle.application;

import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import io.qameta.allure.*;
import jakarta.xml.ws.WebServiceException;
import net.ihe.gazelle.app.patientregistryapi.business.Patient;
import net.ihe.gazelle.app.patientregistryapi.business.PatientSearchCriterionKey;
import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import net.ihe.gazelle.app.patientregistrysearchclient.adapter.PatientSearchClient;
import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;
import net.ihe.gazelle.lib.searchmodelapi.business.SearchCriteria;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.SearchCriterion;
import net.ihe.gazelle.lib.searchmodelapi.business.searchcriterion.StringSearchCriterion;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;


@Feature("PatientSearchClientTest")
@ExtendWith(MockitoExtension.class)
public class PatientRegistrySearchClientTest {

    private static final String TEST_UUID = "123e4567-e89b-12d3-a456-426614174000";
    private static final String MALFORMED_UUID = "123e4567-e89b-12d3-a456-42661417400000000000000000000000000";

    @Mock
    static private PatientSearchClient patientSearchClientMock;
    @Mock
    static private OperationalPreferencesService operationalPreferencesService;
    @Mock
    private PatientRegistrySearchClient patientRegistrySearchClient;

    @BeforeAll
    static void initialize() {
        patientSearchClientMock = Mockito.mock(PatientSearchClient.class);
        operationalPreferencesService = Mockito.mock(OperationalPreferencesService.class);
    }

    @Test //TODO correct this test
    @Disabled
    @Description("Test on initialization, when a namespace exception is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("initialization")
    public void TestInitializeNameSpaceException() throws PreferenceException, NamespaceException {
        Mockito.doThrow(NamespaceException.class).when(operationalPreferencesService).getStringValue("java:/app/gazelle/pixm-connector" +
                "/operational" +
                "-preferences", "patientregistry.url");
        patientRegistrySearchClient = new PatientRegistrySearchClient(operationalPreferencesService);

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion<String> searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(TEST_UUID);
        searchCriteria.addSearchCriterion(searchCriterion);

        assertThrows(SearchException.class,
                () -> patientRegistrySearchClient.searchPatient(searchCriterion.getValue()));
    }

    @Test //TODO correct this test
    @Disabled
    @Description("Test on initialization, when a preference exception is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("initialization")
    public void TestInitializePreferenceException() throws PreferenceException, NamespaceException {
        Mockito.doThrow(PreferenceException.class).when(operationalPreferencesService).getStringValue("java:/app/gazelle/pixm-connector" +
                "/operational" +
                "-preferences", "patientregistry.url");
        patientRegistrySearchClient = new PatientRegistrySearchClient(operationalPreferencesService);

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion<String> searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(TEST_UUID);
        searchCriteria.addSearchCriterion(searchCriterion);

        assertThrows(SearchException.class,
                () -> patientRegistrySearchClient.searchPatient(searchCriterion.getValue()));
    }

    @Test //TODO correct this test
    @Disabled
    @Description("Test on initialization, when the url of the server is malformed")
    @Severity(SeverityLevel.CRITICAL)
    @Story("initialization")
    public void TestInitializeMalformedURLException() throws PreferenceException, NamespaceException {
        Mockito.doThrow(PreferenceException.class).when(operationalPreferencesService).getStringValue("java:/app/gazelle/pixm-connector" +
                "/operational" +
                "-preferences", "patientregistry.url");
        patientRegistrySearchClient = new PatientRegistrySearchClient(operationalPreferencesService);

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion<String> searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(MALFORMED_UUID);
        searchCriteria.addSearchCriterion(searchCriterion);

        assertThrows(SearchException.class,
                () -> patientRegistrySearchClient.searchPatient(searchCriterion.getValue()));
    }

    @Test //TODO correct this test
    @Disabled
    @Description("Test on initialization, when a web service exception is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("initialization")
    public void TestInitializeWebServiceException() throws PreferenceException, NamespaceException {
        Mockito.doThrow(WebServiceException.class).when(operationalPreferencesService).getStringValue("java:/app/gazelle/pixm-connector" +
                "/operational" +
                "-preferences", "patientregistry.url");
        patientRegistrySearchClient = new PatientRegistrySearchClient(operationalPreferencesService);

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion<String> searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(TEST_UUID);
        searchCriteria.addSearchCriterion(searchCriterion);

        assertThrows(SearchException.class,
                () -> patientRegistrySearchClient.searchPatient(searchCriterion.getValue()));
    }

    @Test
    @Description("Test on read, a specific excpetion is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("read")
    public void TestSearchExceptionMapping() throws SearchException {

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion<String> searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(TEST_UUID);
        searchCriteria.addSearchCriterion(searchCriterion);

        SearchException exception = new SearchException("Exception while Mapping with GITB elements !");
        patientRegistrySearchClient = new PatientRegistrySearchClient();
        patientRegistrySearchClient.setClient(patientSearchClientMock);
        Mockito.doThrow(exception).when(patientSearchClientMock).search(any());
        assertThrows(InternalErrorException.class,
                () -> patientRegistrySearchClient.searchPatient(searchCriterion.getValue()));
    }

    @Test
    @Description("Test on read, a specific excpetion is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("read")
    public void TestSearchExceptionInvalidResponse() throws SearchException {

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion<String> searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(TEST_UUID);
        searchCriteria.addSearchCriterion(searchCriterion);

        SearchException exception = new SearchException("Invalid Response from distant PatientFeedProcessingService !");
        Mockito.doThrow(exception).when(patientSearchClientMock).search(any());
        patientRegistrySearchClient = new PatientRegistrySearchClient();
        patientRegistrySearchClient.setClient(patientSearchClientMock);
        assertThrows(ResourceNotFoundException.class,
                () -> patientRegistrySearchClient.searchPatient(searchCriterion.getValue()));
    }

    @Test
    @Description("Test on read, a specific exception is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("read")
    public void TestSearchExceptionInvalidOperation() throws SearchException {

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion<String> searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(TEST_UUID);
        searchCriteria.addSearchCriterion(searchCriterion);

        SearchException exception = new SearchException("Invalid operation used on distant PatientFeedProcessingService !");
        Mockito.doThrow(exception).when(patientSearchClientMock).search(any());
        patientRegistrySearchClient = new PatientRegistrySearchClient();
        patientRegistrySearchClient.setClient(patientSearchClientMock);
        assertThrows(InvalidRequestException.class,
                () -> patientRegistrySearchClient.searchPatient(searchCriterion.getValue()));
    }

    @Test
    @Description("Test on read, a specific exception is thrown")
    @Severity(SeverityLevel.CRITICAL)
    @Story("read")
    public void TestSearchExceptionInvalidRequest() throws SearchException {

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion<String> searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(TEST_UUID);
        searchCriteria.addSearchCriterion(searchCriterion);

        SearchException exception = new SearchException("Invalid Request sent to distant PatientFeedProcessingService !");
        Mockito.doThrow(exception).when(patientSearchClientMock).search(any());
        patientRegistrySearchClient = new PatientRegistrySearchClient();
        patientRegistrySearchClient.setClient(patientSearchClientMock);
        assertThrows(InvalidRequestException.class,
                () -> patientRegistrySearchClient.searchPatient(searchCriterion.getValue()));
    }

    @Test
    @Description("Test on read, when no patient is returned")
    @Severity(SeverityLevel.CRITICAL)
    @Story("read")
    public void TestNoPatientReturned() throws SearchException {

        List<Patient> resources = new ArrayList<>();

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion<String> searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(TEST_UUID);
        searchCriteria.addSearchCriterion(searchCriterion);

        doAnswer(invocation -> resources).when(patientSearchClientMock).search(any());

        patientRegistrySearchClient = new PatientRegistrySearchClient();
        patientRegistrySearchClient.setClient(patientSearchClientMock);
        assertThrows(ResourceNotFoundException.class,
                () -> patientRegistrySearchClient.searchPatient(searchCriterion.getValue()));


    }

    @Test
    @Description("Test on read, when exactly one patient is returned")
    @Severity(SeverityLevel.CRITICAL)
    @Story("read")
    public void TestOnePatientReturned() throws SearchException {

        org.hl7.fhir.r4.model.Patient arthur = new org.hl7.fhir.r4.model.Patient();
        arthur.addName().addGiven("Arthur");

        List<Patient> resources = new ArrayList<>();
        Patient pat = new Patient();
        PersonName ps = new PersonName();
        ps.addGiven("Arthur");
        pat.addName(ps);
        resources.add(pat);

        SearchCriteria searchCriteria = new SearchCriteria();
        SearchCriterion<String> searchCriterion = new StringSearchCriterion(PatientSearchCriterionKey.UUID);
        searchCriterion.setValue(TEST_UUID);
        searchCriteria.addSearchCriterion(searchCriterion);

        doAnswer(invocation -> resources).when(patientSearchClientMock).search(any());

        patientRegistrySearchClient = new PatientRegistrySearchClient();
        patientRegistrySearchClient.setClient(patientSearchClientMock);
        assertEquals(arthur.getName().get(0).getGiven().toString(), patientRegistrySearchClient.searchPatient(searchCriterion.getValue()).getName().get(0).getGiven().toString());

    }

    @Test
    @Description("Test on read, when more than one patient is returned")
    @Severity(SeverityLevel.CRITICAL)
    @Story("read")
    public void TestManyPatientsReturned() throws SearchException {

        List<Patient> resources = new ArrayList<>();
        resources.add(new Patient());
        resources.add(new Patient());


        doAnswer(invocation -> resources).when(patientSearchClientMock).search(any());

        patientRegistrySearchClient = new PatientRegistrySearchClient();
        patientRegistrySearchClient.setClient(patientSearchClientMock);
        assertThrows(ResourceNotFoundException.class,
                () -> patientRegistrySearchClient.searchPatient(TEST_UUID));


    }

    //@Test
    @Description("Test on read, when a conversion exception happens")
    @Severity(SeverityLevel.CRITICAL)
    @Story("read")
    public void TestConversionException() throws SearchException {

        List<Patient> resources = new ArrayList<>();
        resources.add(new Patient());
        resources.add(new Patient());
        patientRegistrySearchClient = new PatientRegistrySearchClient();
        patientRegistrySearchClient.setClient(patientSearchClientMock);
        assertThrows(ResourceNotFoundException.class,
                () -> patientRegistrySearchClient.searchPatient(TEST_UUID));


    }


    @Test
    @Description("Test on read, when parameter is null")
    @Severity(SeverityLevel.CRITICAL)
    @Story("read")
    public void TestNoEntry() throws SearchException {

        patientRegistrySearchClient = new PatientRegistrySearchClient();
        patientRegistrySearchClient.setClient(patientSearchClientMock);

        assertThrows(InvalidRequestException.class,
                () -> patientRegistrySearchClient.searchPatient(null));


    }
}
