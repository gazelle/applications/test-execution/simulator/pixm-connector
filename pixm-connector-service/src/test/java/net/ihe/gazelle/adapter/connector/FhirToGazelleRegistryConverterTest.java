package net.ihe.gazelle.adapter.connector;

import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import io.qameta.allure.*;
import net.ihe.gazelle.app.patientregistryapi.business.AddressUse;
import net.ihe.gazelle.app.patientregistryapi.business.GenderCode;
import net.ihe.gazelle.app.patientregistryapi.business.Person;
import net.ihe.gazelle.app.patientregistryapi.business.PersonName;
import org.hl7.fhir.r4.model.*;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@Feature("FhirToGazelleConverter")
public class FhirToGazelleRegistryConverterTest {

    @Test
    @Description("Test on unitary conversion")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Name conversion")
    void TestPatientNameConversion() {
        Patient pat = new Patient();
        HumanName name = new HumanName();
        name.addGiven("Patrick");
        name.setUse(HumanName.NameUse.TEMP);
        name.setFamily("Timsit");
        name.addPrefix("Jr");
        name.addSuffix("Sr");
        pat.addName(name);

        try {
            net.ihe.gazelle.app.patientregistryapi.business.Patient response = FhirToGazelleRegistryConverter.convertPatient(pat);
            PersonName responseName = response.getNames().get(0);
            assertEquals("Patrick", responseName.getGivens().get(0));
            assertEquals("TEMP", responseName.getUse());
            assertEquals("Timsit", responseName.getFamily());
            assertEquals("Jr", responseName.getPrefix());
            assertEquals("Sr", responseName.getSuffix());
        } catch (ConversionException e) {
            fail("could not convert Patient with just a name");
        }

    }

    @Test
    @Description("Test on unitary conversion")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Gender conversion")
    void TestPatientGenderConversion() {

        Patient pat1 = new Patient();
        pat1.setGender(Enumerations.AdministrativeGender.FEMALE);

        Patient pat2 = new Patient();
        pat2.setGender(Enumerations.AdministrativeGender.MALE);

        Patient pat3 = new Patient();
        pat3.setGender(Enumerations.AdministrativeGender.OTHER);

        Patient pat4 = new Patient();
        pat4.setGender(Enumerations.AdministrativeGender.UNKNOWN);

        try {
            net.ihe.gazelle.app.patientregistryapi.business.Patient response = FhirToGazelleRegistryConverter.convertPatient(pat1);
            assertEquals(GenderCode.FEMALE, response.getGender());

            response = FhirToGazelleRegistryConverter.convertPatient(pat2);
            assertEquals(GenderCode.MALE, response.getGender());

            response = FhirToGazelleRegistryConverter.convertPatient(pat3);
            assertEquals(GenderCode.OTHER, response.getGender());

            response = FhirToGazelleRegistryConverter.convertPatient(pat4);
            assertEquals(GenderCode.UNDEFINED, response.getGender());

        } catch (ConversionException e) {
            fail("could not convert Patient with just a gender");
        }

    }

    @Test
    @Description("Test on unitary conversion")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Contact conversion")
    void TestPatientContactConversion() {

        Patient pat = new Patient();
        Patient.ContactComponent comp = new Patient.ContactComponent();
        comp.setGender(Enumerations.AdministrativeGender.OTHER);
        Address address = new Address();
        address.addLine("Henlo");
        comp.setAddress(address);
        comp.setName(new HumanName().addGiven("toto"));
        pat.addContact(comp);

        try {
            net.ihe.gazelle.app.patientregistryapi.business.Patient response = FhirToGazelleRegistryConverter.convertPatient(pat);
            Person contact = response.getContacts().get(0);
            assertEquals(GenderCode.OTHER, contact.getGender());
            assertEquals("Henlo", contact.getAddresses().get(0).getLines().get(0));
            assertEquals("toto", contact.getNames().get(0).getGivens().get(0));

        } catch (ConversionException e) {
            fail("could not convert Patient with just a contact");
        }

    }

    @Test
    @Description("Test on unitary conversion")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Address conversion")
    void TestPatientAddressConversion() {

        Patient pat = new Patient();
        Address address = new Address();
        address.setCity("Chicoutimi");
        address.setCountry("Canada");
        address.setPostalCode("G7B 0G5");
        address.setState("Québec");
        address.setUse(Address.AddressUse.TEMP);
        address.addLine("4 boulevard de l'Université");
        pat.addAddress(address);

        Patient pat1 = new Patient();
        Address address1 = new Address();
        address1.setUse(Address.AddressUse.BILLING);
        pat1.addAddress(address1);
        Patient pat2 = new Patient();
        Address address2 = new Address();
        address2.setUse(Address.AddressUse.HOME);
        pat2.addAddress(address2);
        Patient pat3 = new Patient();
        Address address3 = new Address();
        address3.setUse(Address.AddressUse.OLD);
        pat3.addAddress(address3);
        Patient pat4 = new Patient();
        Address address4 = new Address();
        address4.setUse(Address.AddressUse.WORK);
        pat4.addAddress(address4);

        try {
            net.ihe.gazelle.app.patientregistryapi.business.Patient response = FhirToGazelleRegistryConverter.convertPatient(pat);
            net.ihe.gazelle.app.patientregistryapi.business.Address responseAddress = response.getAddresses().get(0);
            assertEquals("Chicoutimi", responseAddress.getCity());
            assertEquals("Canada", responseAddress.getCountryIso3());
            assertEquals("G7B 0G5", responseAddress.getPostalCode());
            assertEquals("Québec", responseAddress.getState());
            assertEquals(AddressUse.TEMPORARY, responseAddress.getUse());
            assertEquals("4 boulevard de l'Université", responseAddress.getLines().get(0));

            response = FhirToGazelleRegistryConverter.convertPatient(pat1);
            responseAddress = response.getAddresses().get(0);
            assertEquals(AddressUse.BILLING, responseAddress.getUse());

            response = FhirToGazelleRegistryConverter.convertPatient(pat2);
            responseAddress = response.getAddresses().get(0);
            assertEquals(AddressUse.HOME, responseAddress.getUse());

            response = FhirToGazelleRegistryConverter.convertPatient(pat3);
            responseAddress = response.getAddresses().get(0);
            assertEquals(AddressUse.BAD, responseAddress.getUse());

            response = FhirToGazelleRegistryConverter.convertPatient(pat4);
            responseAddress = response.getAddresses().get(0);
            assertEquals(AddressUse.WORK, responseAddress.getUse());

        } catch (ConversionException e) {
            fail("could not convert Patient with just an address");
        }

    }

    @Test
    @Description("Test on unitary conversion")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Single parameters conversion")
    void TestPatientParametersConversion() {

        LocalDate dateValue = LocalDate.now();

        Patient pat = new Patient();
        pat.setActive(true);
        pat.setBirthDate(java.sql.Date.valueOf(dateValue));
        pat.setId("Hello");

        try {
            net.ihe.gazelle.app.patientregistryapi.business.Patient response = FhirToGazelleRegistryConverter.convertPatient(pat);
            assertEquals(java.sql.Date.valueOf(dateValue), response.getDateOfBirth());
            assertEquals("Hello", response.getUuid());
            assertEquals(true, response.isActive());

        } catch (ConversionException e) {
            fail("could not convert Patient with simple parameters");
        }

    }

    @Test
    @Description("Test on unitary conversion")
    @Severity(SeverityLevel.CRITICAL)
    @Story("Single parameters conversion")
    void TestPatientIdentifiersConversion() {

        Patient pat = new Patient();
        Identifier id = new Identifier();
        id.setSystem("urn:oid:Hello_Vincent");
        id.setValue("69420");
        pat.addIdentifier(id);

        Patient pat1 = new Patient();
        Identifier id1 = new Identifier();
        pat.addIdentifier(id1);


        try {
            net.ihe.gazelle.app.patientregistryapi.business.Patient response = FhirToGazelleRegistryConverter.convertPatient(pat);
            assertEquals("Hello_Vincent", response.getIdentifiers().get(0).getSystemIdentifier());
            assertEquals("69420", response.getIdentifiers().get(0).getValue());

            response = FhirToGazelleRegistryConverter.convertPatient(pat1);

        } catch (InvalidRequestException e) {
            assertEquals("Cannot create Patient without any Identifier", e.getMessage());
        } catch (ConversionException e) {
            fail();
        }

    }

}
