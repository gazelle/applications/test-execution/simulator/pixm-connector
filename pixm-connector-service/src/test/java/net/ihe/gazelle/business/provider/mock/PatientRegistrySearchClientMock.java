package net.ihe.gazelle.business.provider.mock;

import net.ihe.gazelle.application.PatientRegistrySearchClient;
import net.ihe.gazelle.lib.searchmodelapi.business.exception.SearchException;
import org.hl7.fhir.r4.model.Patient;

public class PatientRegistrySearchClientMock extends PatientRegistrySearchClient {
    public static final String GIVEN_NAME = "Arthur";

    public PatientRegistrySearchClientMock() {
    }

    @Override
    public Patient searchPatient(String uuid) throws SearchException {

        Patient arthur = new Patient();
        arthur.addName().addGiven(GIVEN_NAME);

        return switch (uuid) {
            case "1" -> throw new SearchException("Test exception");
            case "3" -> arthur;
            default -> null;
        };
    }
}
