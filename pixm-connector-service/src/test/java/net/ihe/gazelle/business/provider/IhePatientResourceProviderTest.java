package net.ihe.gazelle.business.provider;

import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.param.*;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.UnprocessableEntityException;
import jakarta.servlet.http.HttpServletRequest;
import net.ihe.gazelle.adapter.connector.FhirToGazelleRegistryConverter;
import net.ihe.gazelle.application.ConfigurationAdapter;
import net.ihe.gazelle.application.PatientRegistryFeedClient;
import net.ihe.gazelle.application.PatientRegistrySearchClient;
import net.ihe.gazelle.application.PatientRegistryXRefSearchClient;
import net.ihe.gazelle.business.provider.mock.*;
import net.ihe.gazelle.business.service.RequestValidatorService;
import org.hl7.fhir.r4.model.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Collectors;

import static net.ihe.gazelle.business.provider.IhePatientResourceProvider.NO_IDENTIFIER_PROVIDED;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class IhePatientResourceProviderTest {

    public static final String UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN = "Unprocessable Entity Exception has to be thrown";
    public static final String INVALID_REQUEST_EXCEPTION_HAS_TO_BE_THROWN = "InvalideRequestException has to be thrown";
    public static final String ERROR_IN_DELETION = "Error in deletion";
    private static PatientRegistryXRefSearchClient xRefSearchClientMock;
    private static PatientRegistrySearchClient searchClientMock;
    private static RequestValidatorService rvs;
    private static PatientRegistryFeedClient patientRegistryFeedClient;
    private static ConfigurationAdapter configuration;
    private static IhePatientResourceProvider provider;
    private static final HttpServletRequest iti104Request = createHttpServletRequest("src/test/resources/http_request_iti104.http");
    static final String patientString = convertReaderIntoString(iti104Request);
    private static final HttpServletRequest iti83GetRequest = createHttpServletRequest("src/test/resources/http_request_get_iti83.http");


    @BeforeAll
    static void initialized() {
        xRefSearchClientMock = new PatientRegistryXRefSearchClientMock();
        searchClientMock = new PatientRegistrySearchClientMock();
        rvs = new RequestValidatorServiceMock();
        configuration = new ConfigurationAdapterMock();
        patientRegistryFeedClient = new PatientRegistryFeedClientMock();
        provider = new IhePatientResourceProvider(xRefSearchClientMock, searchClientMock, patientRegistryFeedClient, rvs, configuration);
    }

    @Test
    void testCreatePatient() {
        //Given
        Patient patient = provider.createFhirResourceFromString(Patient.class, patientString);
        //When
        MethodOutcome mo = provider.create(patient, iti104Request);
        //Then
        assertTrue(mo.getCreated());
    }

    @Test
    void testCreatePatientWithFailedValidationReport() {
        //Given
        Patient patient = provider.createFhirResourceFromString(Patient.class, patientString);
        patient.setId("FAILED_VALIDATION");
        try {
            //When
            provider.create(patient, iti104Request);
        } catch (InvalidRequestException e) {
            //Then
            OperationOutcome oo = (OperationOutcome) e.getOperationOutcome();
            assertTrue(oo.getIssue().get(0).getDiagnostics().contains("HAPI-0450"));
        } catch (Exception e) {
            fail(INVALID_REQUEST_EXCEPTION_HAS_TO_BE_THROWN, e);
        }
    }

    @Test
    void testCreatePatientWithNull() {
        //Given
        Patient patient = provider.createFhirResourceFromString(Patient.class, patientString);
        patient.setId("PatientFeedException");
        try {
            //When
            provider.create(null, iti104Request);
        } catch (UnprocessableEntityException e) {
            //Then
            assertEquals(IhePatientResourceProvider.NO_PATIENT_PROVIDED, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }
    }

    @Test
    void testCreatePatientWithPatientFeedException() {
        //Given
        Patient patient = provider.createFhirResourceFromString(Patient.class, patientString);
        patient.setId("PatientFeedException");
        try {
            //When
            provider.create(patient, iti104Request);
        } catch (UnprocessableEntityException e) {
            //Then
            assertEquals(IhePatientResourceProvider.PATIENT_FEED_CLIENT_IS_NOT_SET, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }
    }

    @Test
    void testCreatePatientWithConversionException() {
        //Given
        Patient patient = provider.createFhirResourceFromString(Patient.class, patientString);
        patient.setId("ConversionException");
        try {
            //When
            provider.create(patient, iti104Request);
        } catch (UnprocessableEntityException e) {
            //Then
            assertEquals(IhePatientResourceProvider.FHIR_PATIENT_COULD_NOT_BE_CONVERTED_TO_REGISTRY_PATIENT, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }

    }

    @Test
    void testCreatePatientWithNullCreatedPatientFromRegistry() {
        //Given
        Patient patient = provider.createFhirResourceFromString(Patient.class, patientString);
        patient.setId("null");
        try {
            //When
            provider.create(patient, iti104Request);
        } catch (UnprocessableEntityException e) {
            //Then
            assertEquals(IhePatientResourceProvider.PROBLEM_DURING_CREATING_PATIENT_IN_PATIENT_REGISTRY, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }
    }

    @Test
    void testUpdatePatient() {
        //Given
        Patient patient = provider.createFhirResourceFromString(Patient.class, patientString);
        String theConditional = "Patient?identifier=system%7C00001";
        //When
        MethodOutcome mo = provider.update(null, theConditional, patient, iti104Request);
        //Then
        Patient patientReturned = (Patient) mo.getResource();
        assertEquals("PatientIsUpdated", patientReturned.getId());
    }

    @Test
    void testUpdatePatientWithPatientFeedException() {
        //Given
        Patient patient = provider.createFhirResourceFromString(Patient.class, patientString);
        String theConditional = "Patient?identifier=system%7C00001";

        //When
        try {
            //When
            provider.update(null, theConditional, patient, iti104Request);
        } catch (UnprocessableEntityException e) {
            //Then
            assertEquals(IhePatientResourceProvider.PATIENT_FEED_CLIENT_IS_NOT_SET, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }
    }

    @Test
    void testUpdatePatientWithConversionException() {
        //Given
        Patient patient = provider.createFhirResourceFromString(Patient.class, patientString);
        String theConditional = "Patient?identifier=system%7CConversionException";
        try {
            //When
            provider.update(null, theConditional, patient, iti104Request);
        } catch (UnprocessableEntityException e) {
            //Then
            assertEquals(IhePatientResourceProvider.FHIR_PATIENT_COULD_NOT_BE_CONVERTED_TO_REGISTRY_PATIENT, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }
    }

    @Test
    void testUpdatePatientWithNullConditional() {
        //Given
        Patient patient = provider.createFhirResourceFromString(Patient.class, patientString);
        try {
            //When
            provider.update(null, null, patient, iti104Request);
        } catch (UnprocessableEntityException e) {
            //Then
            assertEquals(NO_IDENTIFIER_PROVIDED, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }
    }

    @Test
    void testReadOk() {
        //Given
        IdType id = new IdType();
        id.setValue("3");
        //When
        Patient p = provider.read(id);
        //Then
        assertEquals(PatientRegistrySearchClientMock.GIVEN_NAME, p.getName().get(0).getGivenAsSingleString());
    }

    @Test
    void testReadThrows() {
        //Given
        IdType id = new IdType();
        id.setValue("1");
        try {
            //When
            provider.read(id);
        } catch (UnprocessableEntityException e) {
            //Then
            assertEquals(IhePatientResourceProvider.PATIENT_COULD_NOT_BE_RETRIEVED, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }
    }

    @Test
    void testReadNull() {
        IdType id = new IdType();
        try {
            //When
            provider.read(id);
        } catch (UnprocessableEntityException e) {
            //Then
            assertEquals(NO_IDENTIFIER_PROVIDED, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }
    }

    @Test
    void testGetResource() {
        assertEquals(Patient.class, provider.getResourceType());
    }

    @Test
    void testDelete() {
        //Given
        IdType id = new IdType(40);
        String theConditional = "Patient?identifier=system%7C00001";

        //When
        MethodOutcome mo = provider.delete(id, theConditional, iti104Request);
        //Then
        assertEquals(200, mo.getResponseStatusCode());
    }

    @Test
    void testDeleteWithNoSystemValueForIdentifier() {
        //Given
        IdType id = new IdType();
        String theConditional = "Patient?identifier=";
        try {
            //when
            Assertions.assertThrows(UnprocessableEntityException.class, () -> provider.delete(id, theConditional, iti104Request));
        } catch (UnprocessableEntityException e) {
            //Then
            assertEquals(ERROR_IN_DELETION, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }
    }

    @Test
    void testDeleteWithErrorDuringDeletion() {
        //Given
        IdType id = new IdType(42);
        String theConditional = "Patient?identifier=system%7CFAILED";

        try {
            //When
            provider.delete(id, theConditional, iti104Request);
        } catch (UnprocessableEntityException e) {
            //Then
            assertTrue(e.getCause().getMessage().contains(PatientRegistryFeedClientMock.CANNOT_DELETE_PATIENT_WITH_IDENTIFIER));
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }
    }

    @Test
    void testDeleteWithNullReturned() {
        //Given
        IdType id = new IdType(42);
        String theConditional = "Patient?identifier=system%7CNULL";

        //When
        MethodOutcome mo = provider.delete(id, theConditional, iti104Request);
        //Then
        assertEquals(204, mo.getResponseStatusCode());
    }

    @Test
    void testDeleteWithNullIdentifier() {
        //Given
        IdType id = new IdType(42);
        String theConditional = "";
        try {
            //When
            provider.delete(id, theConditional, iti104Request);
        } catch (UnprocessableEntityException e) {
            //Then
            assertEquals(NO_IDENTIFIER_PROVIDED, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }
    }


    @Test
    void findPatientsByIdentifierUrn() {
        //Given
        String system = "urn:oid:1";
        String value = "69420";
        TokenAndListParam tokenAndListParam = createTokenAndListParam(system, value);
        String value2 = "urn:oid:2";
        StringAndListParam targetSystem = createTokenAndListParam(value2);
        //When
        Parameters parameters = provider.findCorrespondingIdentifiersWithGet(tokenAndListParam, targetSystem, iti83GetRequest);

        //Then
        Parameters.ParametersParameterComponent singleResponse = parameters.getParameter().get(0);
        Identifier r4 = (Identifier) singleResponse.getValue();
        assertEquals(PatientRegistryXRefSearchClientMock.URN_OK, r4.getValue());
        assertEquals(PatientRegistryXRefSearchClientMock.URN_OK, r4.getSystem());
    }

    @Test
    public void findPatientsByIdentifierHttp() {
        //Given
        String system = "http://1";
        String value = "69420";
        TokenAndListParam tokenAndListParam = createTokenAndListParam(system, value);
        String value2 = "http://2";
        StringAndListParam targetSystem = createTokenAndListParam(value2);
        //When
        Parameters response = provider.findCorrespondingIdentifiersWithGet(tokenAndListParam, targetSystem, iti83GetRequest);

        //Then
        Parameters.ParametersParameterComponent singleResponse = response.getParameter().get(0);
        Identifier r4 = (Identifier) singleResponse.getValue();
        assertEquals(PatientRegistryXRefSearchClientMock.HTTP_OK, r4.getValue());
        assertEquals(PatientRegistryXRefSearchClientMock.HTTP_OK, r4.getSystem());

    }

    @Test
    void findPatientsByIdentifierNull() {
        //Given
        String value2 = "http://2";
        StringAndListParam targetSystem = createTokenAndListParam(value2);
        try {
            //When
            provider.findCorrespondingIdentifiersWithGet(null, targetSystem, iti83GetRequest);
        } catch (UnprocessableEntityException e) {
            //Then
            assertEquals(IhePatientResourceProvider.SOURCE_IDENTIFIER_PATIENT_IDENTIFIER_NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }
    }

    @Test
    void findPatientsByNullTarget() {
        //Given
        String system = "http://1";
        String value = "69420";
        TokenAndListParam tokenAndListParam = createTokenAndListParam(system, value);
        try {
            //When
            provider.findCorrespondingIdentifiersWithGet(tokenAndListParam, null, iti83GetRequest);
        } catch (UnprocessableEntityException e) {
            //Then
            assertEquals(PatientRegistryXRefSearchClient.TARGET_SYSTEM_NOT_FOUND, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }
    }

    @Test
    public void findPatientsByIdentifierWithoutHttpNorUrnOid() {
        //Given
        String system = "0";
        String value = "69420";
        TokenAndListParam tokenAndListParam = createTokenAndListParam(system, value);
        String value2 = "http://2";
        StringAndListParam targetSystem = createTokenAndListParam(value2);

        try {
            //When
            provider.findCorrespondingIdentifiersWithGet(tokenAndListParam, targetSystem, iti83GetRequest);
        } catch (UnprocessableEntityException e) {
            //Then
            assertEquals(FhirToGazelleRegistryConverter.SOURCE_IDENTIFIER_PATIENT_IDENTIFIER_NOT_HTTP_NOR_URN_OID, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }

    }

    @Test
    public void findPatientsByIdentifierWithSearchCrossReferenceException() {
        //Given
        String system = "urn:oid:0";
        String value = "69420";
        TokenAndListParam tokenAndListParam = createTokenAndListParam(system, value);
        String value2 = "http://2";
        StringAndListParam targetSystem = createTokenAndListParam(value2);

        try {
            //When
            provider.findCorrespondingIdentifiersWithGet(tokenAndListParam, targetSystem, iti83GetRequest);
        } catch (UnprocessableEntityException e) {
            //Then
            assertEquals(PatientRegistryXRefSearchClientMock.ERROR_DURING_SEARCH_CROSS_REFERENCE, e.getMessage());
        } catch (Exception e) {
            fail(UNPROCESSABLE_ENTITY_EXCEPTION_HAS_TO_BE_THROWN);
        }

    }


    ////////////////////////////////////////////////////////////////
    // Methods used privately in the exclusive usage of Test Class //
    ////////////////////////////////////////////////////////////////


    private TokenAndListParam createTokenAndListParam(String system, String value) {
        return new TokenAndListParam().addAnd(new TokenParam(system, value));
    }

    private StringAndListParam createTokenAndListParam(String value) {
        StringAndListParam targetDomains = new StringAndListParam();
        StringOrListParam stringParam = new StringOrListParam();
        stringParam.add(new StringParam(value));
        targetDomains.addAnd(stringParam);
        return targetDomains;
    }

    private static String convertReaderIntoString(HttpServletRequest request) {
        try {
            return request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        } catch (IOException e) {
            throw new IllegalStateException();
        }
    }

    private static HttpServletRequest createHttpServletRequest(String path) {
        HttpServletRequest request;
        try {
            String content = Files.readString(Path.of(path), StandardCharsets.UTF_8);
            request = new HttpServletRequestMock(content);
        } catch (HttpFormatException | IOException e) {
            throw new RuntimeException(e);
        }
        return request;

    }
}

