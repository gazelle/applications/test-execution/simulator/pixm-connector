package net.ihe.gazelle.business.provider.mock;

import ca.uhn.fhir.rest.server.exceptions.InternalErrorException;
import ca.uhn.fhir.rest.server.exceptions.InvalidRequestException;
import ca.uhn.fhir.rest.server.exceptions.ResourceGoneException;
import net.ihe.gazelle.adapter.connector.ConversionException;
import net.ihe.gazelle.adapter.connector.GazelleRegistryToFhirConverter;
import net.ihe.gazelle.app.patientregistryapi.application.PatientFeedException;
import net.ihe.gazelle.app.patientregistryapi.business.EntityIdentifier;
import net.ihe.gazelle.app.patientregistryfeedclient.adapter.PatientFeedClient;
import net.ihe.gazelle.application.PatientRegistryFeedClient;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import org.hl7.fhir.r4.model.Patient;

public class PatientRegistryFeedClientMock extends PatientRegistryFeedClient {

    public static final String HTTP_OK = "HTTP_OK";
    public static final String URN_OK = "URN_OK";
    private static final String CROSS_REFERENCE = "Cross Reference";

    public static final String CANNOT_DELETE_PATIENT_WITH_IDENTIFIER = "Cannot delete patient with identifier ";

    public PatientRegistryFeedClientMock() {
    }

    public PatientRegistryFeedClientMock(OperationalPreferencesService operationalPreferencesService) {
        super(operationalPreferencesService);
    }

    public PatientRegistryFeedClientMock(PatientFeedClient searchClient) {
        this.setClient(searchClient);
    }

    @Override
    public Patient createPatient(net.ihe.gazelle.app.patientregistryapi.business.Patient patient) throws PatientFeedException, ConversionException {

        String uuid = patient.getUuid();
        return switch (uuid) {
            case "PatientFeedException" -> throw new PatientFeedException();
            case "ConversionException" -> throw new ConversionException();
            default -> (Patient) new Patient().setId("1");
        };
    }

    @Override
    public Patient updatePatient(net.ihe.gazelle.app.patientregistryapi.business.Patient patientToUpdate, EntityIdentifier identifier) throws PatientFeedException, ConversionException {
        return switch (identifier.getValue()) {
            case "PatientFeedException" -> throw new PatientFeedException();
            case "ConversionException" -> throw new ConversionException();
            default -> {
                Patient patient = GazelleRegistryToFhirConverter.convertPatient(patientToUpdate);
                patient.setId("PatientIsUpdated");
                yield patient;
            }
        };
    }

    @Override
    public EntityIdentifier delete(EntityIdentifier identifier){

        if (identifier == null || identifier.getValue() == null) {
            throw new InvalidRequestException("Invalid identifier");
        }

        if ("FAILED".equals(identifier.getValue())) {
            throw new ResourceGoneException(CANNOT_DELETE_PATIENT_WITH_IDENTIFIER + identifier);
        }

        if ("NULL".equals(identifier.getValue())) {
            identifier = null;
        }


        return identifier;
    }

}
